import React, { Component } from 'react';
import { Table, Space, Modal, Button } from 'antd';
import { Link } from 'react-router-dom';
import moment from 'moment';
import Swal from 'sweetalert2';
import apiCaller from '../../../tools/apiUtils';
import '../../../styles/MainContainer.scss';

class PowerIndexPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      psu: []
    };
  }

  componentDidMount() {
    this.getPSU();
  }

  async getPSU() {
    try {
      const res = await apiCaller.get('api/psu/');
      const psu = res.data.payload;
      this.setState({ psu });
      console.log('psu Items', psu)
    } catch (error) {
      console.log(error);
    }
  };

  deletePSU(psuId, psuName) {
    const id = psuId;
    const name = psuName;
    Modal.confirm({
      title: `Are you sure to remove: ${name}`,
      okText: 'Yes, delete',
      okType: 'danger',
      cancelText: 'No',
      onOk: async() => {
        await apiCaller.delete(`api/psu/delete/${id}`);
        Swal.fire({
          icon: 'success',
          title: 'Delete Success',
          text: `${name} deleted sucessfully!`
        });
        const res = await apiCaller.get('api/psu/');
        const psu = res.data.payload;
        this.setState({ psu });
      }
    });
  };

  render(){
    const { psu } = this.state;

    const columns = [
      {
        title: '#',
        dataIndex: 'id',
        width: '5%'
      },
      {
        title: 'Name',
        dataIndex: 'name',
        width: '30%'
      },
      {
        title: 'Brand',
        dataIndex: 'brand',
        width: '15%'
      },
      {
        title: 'Created At',
        dataIndex: 'createdAt',
        width: '13%',
        render: createdAt => (
          moment(createdAt).format('LL')
        )
      },
      {
        title: 'Price',
        dataIndex: 'price',
        width: '13%',
        render: price => (
          `RM ${price}`
        )
      },
      {
        title: 'Action',
        width: '20%',
        render: psu => (
          <span>
            <Space>
              <Link to={`/power/edit/${psu.id}`}>
                <Button type="primary" shape="round">
                  Edit & View
                </Button>
              </Link>
              <Button type="danger" shape="round" onClick={() => this.deletePSU(psu.id, psu.name)}>
                Delete
              </Button>
            </Space>
          </span>
        )
      }
    ]

    console.log('psu Items', psu);
    return(
      <div className="content">
        <div className="banner">
          <h1>Manage PC Parts: Power Supply Unit (PSU)</h1> 
        </div>
        <div className="createButton">
          <Link to='/power/create'>
            <button type="button" className="btn btn-success">Add a PSU</button>
          </Link>
        </div>
        <div className="indexTable">
          <Table columns={columns} dataSource={psu} pagination={{ defaultPageSize: 12 }} />
        </div>
      </div>
    );
  }
}



export default PowerIndexPage;



