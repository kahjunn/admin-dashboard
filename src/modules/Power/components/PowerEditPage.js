import React, { Component } from 'react';
import { Button, Form, Col } from 'react-bootstrap';
import { Formik, Field, ErrorMessage } from 'formik';
import apiCaller from '../../../tools/apiUtils';
import Toast from '../../../tools/toaster';
import * as yup from 'yup';
import { Link, withRouter } from 'react-router-dom';
import '../../../styles/MainContainer.scss'
import '../styles/PowerForm.scss';

const { Group, Label, Control, Row } = Form;
const { Feedback } = Form.Control;

const psuSchema = yup.object().shape({
  name: yup.string().required("*Field is required"),
  brand: yup.string().required("*Field is required"),
  model: yup.string().required("*Field is required"),
  wattage: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  modularity: yup.string().required("*Selection is required"),
  rating: yup.string().required("*Selection is required"),
  color: yup.string().required("*Field is required"),
  sataConnector: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  pcieConnector: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  molexConnector: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  price: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  detail1: yup.string().required("*Field is required"),
  detail2: yup.string().required("*Field is required"),
  detail3: yup.string().required("*Field is required"),
});

class PowerEditPage extends Component {
  constructor(props) {
    super(props); 
    this.state = {
      psuId: props.match.params.id,
      psu: {}
    };
  }

  componentDidMount() {
    this.getPSU();
  }

  async getPSU() {
    const res = await apiCaller.get(`/api/psu/${this.state.psuId}`);
    const psu = res.data.payload;
    this.setState({ psu });
  }
  
  handleSubmit = async(values) => {
    try {
      const formData = { ...values };
      console.log('FormData to be submitted', formData);
      await apiCaller.put(`api/psu/update/${this.state.psuId}`, formData);
      const res = await apiCaller.get(`/api/psu/${this.state.psuId}`);
      const psu = res.data.payload;
      this.setState({ psu });
      Toast.fire({
        icon: 'success',
        title: `Update Success: ${this.state.psu.name}`
      })
      this.props.history.push('/power');
    } catch (error) {
      console.log("Failed to submit");
      throw error;
    }
  };

  render() {
    const { psu } = this.state;
    return (
      <div className="psu-content">
        <div className="banner-create">
          <h1>Edit Power Supply Unit (PSU): {psu.name}</h1>
        </div>
        <div className="backButton">
          <Link to="/power">
            <Button variant="warning" size="sm">Back</Button>
          </Link>
        </div>

        <div className="create-form">
          <Formik
            initialValues={psu}
            enableReinitialize
            validationSchema={psuSchema}
            onSubmit={(values, {setSubmitting, resetForm}) => {
              console.log("Form values: ", values);
              this.handleSubmit(values);
              setSubmitting(true);
              // resetForm();
              setSubmitting(false);
            }}
          >
            {({ touched, errors, values, handleChange, handleBlur, handleSubmit, isSubmitting }) => (
              <Form onSubmit={handleSubmit}>
              {console.log(values)}
              <Row>
                <Group as={Col} md="4">
                  <Label>Name</Label>
                  <Control
                    type="text"
                    name="name"
                    size="sm"
                    placeholder="Name"
                    onChange={handleChange}
                    value={values.name}
                    isInvalid={!!errors.name}
                  />
                  {touched.name && errors.name ? <Feedback type="invalid">{errors.name}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Brand</Label>
                  <Control
                    type="text"
                    name="brand"
                    size="sm"
                    placeholder="Brand"
                    onChange={handleChange}
                    value={values.brand}
                    isInvalid={!!errors.brand}
                  />
                  {touched.brand && errors.brand ? <Feedback type="invalid">{errors.brand}</Feedback> : null}
                </Group>   
                <Group as={Col} md="4">
                  <Label>Model</Label>
                  <Control
                    type="text"
                    name="model"
                    size="sm"
                    placeholder="Model"
                    onChange={handleChange}
                    value={values.model}
                    isInvalid={!!errors.model}
                  />
                  {touched.model && errors.model ? <Feedback type="invalid">{errors.model}</Feedback> : null}
                </Group>   
              </Row>
              <Row className="mt-2"> 
                <Group as={Col} md="2">
                  <Label>Wattage (W)</Label>
                  <Control
                    type="text"
                    name="wattage"
                    size="sm"
                    placeholder="Wattage (W)"
                    onChange={handleChange}
                    value={values.wattage}
                    isInvalid={!!errors.wattage}
                  />
                  {touched.wattage && errors.wattage ? <Feedback type="invalid">{errors.wattage}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Modularity</Label>
                  <Control
                    as="select"
                    name="modularity"
                    size="sm"
                    value={values.modularity}
                    onChange={handleChange}
                    isInvalid={!!errors.modularity}
                  >
                    <option selected disabled>Choose...</option>
                    <option value="Fully Modular">Fully Modular</option>
                    <option value="Semi-Modular">Semi-Modular</option>
                    <option value="Not Modular">Not Modular</option>
                  </Control>
                  {touched.modularity && errors.modularity ? <Feedback type="invalid">{errors.modularity}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Rating</Label>
                  <Control
                    as="select"
                    name="rating"
                    size="sm"
                    value={values.rating}
                    onChange={handleChange}
                    isInvalid={!!errors.rating}
                  >
                    <option selected disabled>Choose...</option>
                    <option value="80+">80+</option>
                    <option value="80+ Bronze">80+ Bronze</option>
                    <option value="80+ Silver">80+ Silver</option>
                    <option value="80+ Gold">80+ Gold</option>
                    <option value="80+ Platinum">80+ Platinum</option>
                    <option value="80+ Titanium">80+ Titanium</option>
                  </Control>
                  {touched.rating && errors.rating ? <Feedback type="invalid">{errors.rating}</Feedback> : null}
                </Group>
                <Group as={Col} md="3">
                  <Label>Color</Label>
                  <Control
                    type="text"
                    name="color"
                    size="sm"
                    placeholder="Color"
                    onChange={handleChange}
                    value={values.color}
                    isInvalid={!!errors.color}
                  />
                  {touched.color && errors.color ? <Feedback type="invalid">{errors.color}</Feedback> : null}
                </Group>
              </Row >
              <Row className="mt-2">
                <Group as={Col} md="3">
                  <Label>SATA Connectors (No.)</Label>
                  <Control
                    type="text"
                    name="sataConnector"
                    size="sm"
                    placeholder="SATA Connectors (No.)"
                    onChange={handleChange}
                    value={values.sataConnector}
                    isInvalid={!!errors.sataConnector}
                  />
                  {touched.sataConnector && errors.sataConnector ? <Feedback type="invalid">{errors.sataConnector}</Feedback> : null}
                </Group>
                <Group as={Col} md="3">
                  <Label>PCIE 6+2-Pin Connectors (No.)</Label>
                  <Control
                    type="text"
                    name="pcieConnector"
                    size="sm"
                    placeholder="PCIE 6+2-Pin Connectors (No.)"
                    onChange={handleChange}
                    value={values.pcieConnector}
                    isInvalid={!!errors.pcieConnector}
                  />
                  {touched.pcieConnector && errors.pcieConnector ? <Feedback type="invalid">{errors.pcieConnector}</Feedback> : null}
                </Group>
                <Group as={Col} md="3">
                  <Label>Molex 4-Pin Connectors (No.)</Label>
                  <Control
                    type="text"
                    name="molexConnector"
                    size="sm"
                    placeholder="Molex 4-Pin Connectors (No.)"
                    onChange={handleChange}
                    value={values.molexConnector}
                    isInvalid={!!errors.molexConnector}
                  />
                  {touched.molexConnector && errors.molexConnector ? <Feedback type="invalid">{errors.molexConnector}</Feedback> : null}
                </Group>
              </Row>
              <Row className="mt-2">
                <Group as={Col} md="3">
                  <Label>Price (RM)</Label>
                  <Control
                    type="text"
                    name="price"
                    size="sm"
                    placeholder="Price"
                    onChange={handleChange}
                    value={values.price}
                    isInvalid={!!errors.price}
                  />
                  {touched.price && errors.price ? <Feedback type="invalid">{errors.price}</Feedback> : null}
                </Group>
              </Row>
              <Row>
                <Col md="5">
                  <Group>
                    <Label>Details 1</Label>
                    <Control
                      type="text"
                      name="detail1"
                      size="sm"
                      placeholder="Details 1"
                      onChange={handleChange}
                      value={values.detail1}
                      isInvalid={!!errors.detail1}
                    />
                    {touched.detail1 && errors.detail1 ? <Feedback type="invalid">{errors.detail1}</Feedback> : null}
                  </Group>
                  <Group>
                    <Label>Details 2</Label>
                    <Control
                      type="text"
                      name="detail2"
                      size="sm"
                      placeholder="Details 2"
                      onChange={handleChange}
                      value={values.detail2}
                      isInvalid={!!errors.detail2}
                    />
                    {touched.detail2 && errors.detail2 ? <Feedback type="invalid">{errors.detail2}</Feedback> : null}
                  </Group>
                  <Group>
                    <Label>Details 3</Label>
                    <Control
                      type="text"
                      name="detail3"
                      size="sm"
                      placeholder="Details 3"
                      onChange={handleChange}
                      value={values.detail3}
                      isInvalid={!!errors.detail3}
                    />
                    {touched.detail3 && errors.detail3 ? <Feedback type="invalid">{errors.detail3}</Feedback> : null}
                  </Group>
                </Col>
              </Row>
              <Row className="mt-4">
                <Col>
                  <Button variant="primary" type="submit">
                    Submit
                  </Button>
                </Col>
              </Row>
              </Form>           
            )}
          </Formik>
        </div>
      </div>
    );
  }
}

export default PowerEditPage;
