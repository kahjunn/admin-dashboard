import React from 'react';
import { NavLink } from 'react-router-dom';
import USER from '../../../assets/sidebar/user.svg';
import CPU from '../../../assets/sidebar/cpu.svg';
import GPU from '../../../assets/sidebar/gpu.svg';
import MOBO from '../../../assets/sidebar/motherboard.svg';
import RAM from '../../../assets/sidebar/ram.svg';
import STORAGE from '../../../assets/sidebar/storage.svg';
import COOLER from '../../../assets/sidebar/cooler.svg';
import POWER from '../../../assets/sidebar/power.svg';
import CASE from '../../../assets/sidebar/case.svg';
import BUILD from '../../../assets/sidebar/desktop.svg';
import ORDER from '../../../assets/sidebar/order.svg';
import PROMO from '../../../assets/sidebar/promotion.svg';
import '../styles/Sidebar.scss';

const Sidebar = () => {
  
  const viewHeight = window.outerHeight;

  return (
    <div className="sidebar">
      <NavLink to="/admin"><img className="sidebar-logo" src={USER} />Admin Users</NavLink>
      <NavLink to="/cpu"><img className="sidebar-logo" src={CPU} />CPU</NavLink>
      <NavLink to="/gpu"><img className="sidebar-logo" src={GPU} />GPU</NavLink>
      <NavLink to="/mobo"><img className="sidebar-logo" src={MOBO} />Motherboard</NavLink>
      <NavLink to="/ram"><img className="sidebar-logo" src={RAM} />RAM</NavLink>
      <NavLink to="/storage"><img className="sidebar-logo" src={STORAGE} />Storage</NavLink>
      <NavLink to="/power"><img className="sidebar-logo" src={POWER} />PSU</NavLink>
      <NavLink to="/cooler"><img className="sidebar-logo" src={COOLER} />CPU Cooler</NavLink>
      <NavLink to="/casing"><img className="sidebar-logo" src={CASE} />PC Casing</NavLink>
      <NavLink to="/pcbuilds"><img className="sidebar-logo" src={BUILD} />PC Builds</NavLink>
      <NavLink to="/bo"><img className="sidebar-logo" src={ORDER} />Build Orders</NavLink>
      <NavLink to="/promotion"><img className="sidebar-logo" src={PROMO} />Promotions</NavLink>
    </div>
  )
}

export default Sidebar;