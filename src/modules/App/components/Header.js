import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import Logo from '../../../assets/inventory.svg';
import Profile from '../../../assets/profile.svg';
import '../styles/Header.scss';

class Header extends Component {
  render() {
    return (
      <div>
        <header>  
        <div className="header">
              <div className="header-brand col-4">
                <span className="logo"><Link to="/user"><img src={Logo} /></Link></span>
                <span className="brand-name"><Link to="/user">Inventory Stock Management System</Link></span>    
              </div>
              <div className="auth-header col-4">
                <span className="profile-logo">
                  <img src={Profile} />
                  <div className="dropdown-content">
                    <div className="droplink"><Link to="/logout">Log Out</Link></div>
                  </div>
                </span>
              </div>
            </div> 
        </header>
      </div>
    );
  }
}

export default Header;