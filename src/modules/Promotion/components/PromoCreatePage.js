import React, { Component } from 'react';
import { Button, Form, Col } from 'react-bootstrap';
import { Formik, Field, ErrorMessage } from 'formik';
import apiCaller from '../../../tools/apiUtils';
import Toast from '../../../tools/toaster';
import * as yup from 'yup';
import { Link, withRouter } from 'react-router-dom';
import '../../../styles/MainContainer.scss'
import '../styles/PromoForm.scss';
import Thumb from './Thumb';

const { Group, Label, Control, Row } = Form;
const { Feedback } = Form.Control;

const promoSchema = yup.object().shape({
  name: yup.string().required("*Field is required"),
});

class PromoCreatePage extends Component {

  handleSubmit = async(values) => {
    try {
      const formData = { ...values };
      console.log('FormData to be submitted', formData);
      await apiCaller.post('api/promotion/create', formData);
      Toast.fire({
        icon: 'success',
        title: `Promotion: ${formData.name} , added successfully`
      })
      this.props.history.push('/promotion');
    } catch (error) {
      console.log("Failed to submit");
      throw error;
    }
  };

  render() {
    return (
      <div className="promotion-content">
        <div className="banner-create">
          <h1>Add New Promotion</h1>
        </div>
        <div className="backButton">
          <Link to="/promotion">
            <Button variant="warning" size="sm">Back</Button>
          </Link>
        </div>

        <div className="create-form">
          <Formik
            initialValues={{
              name: "",
              file: ""
            }}
            validationSchema={promoSchema}
            onSubmit={(values, {setSubmitting, resetForm}) => {
              console.log("Form values: ", values);
              this.handleSubmit(values);
              setSubmitting(true);
              // resetForm();
              setSubmitting(false);
            }}
          >
            {({ touched, errors, values, handleChange, handleBlur, handleSubmit, isSubmitting, setFieldValue }) => (
              <Form onSubmit={handleSubmit}>
              {console.log(values)}
              <Row>
                <Group as={Col} md="6">
                  <Label>Promotion Name</Label>
                  <Control
                    type="text"
                    name="name"
                    size="sm"
                    placeholder="Promotion Name"
                    onChange={handleChange}
                    value={values.name}
                    isInvalid={!!errors.name}
                  />
                  {touched.name && errors.name ? <Feedback type="invalid">{errors.name}</Feedback> : null}
                </Group>
              </Row>
              <Row className="mt-4">
                <Group as={Col} md="4">
                  <Label>Upload Image</Label>
                  <Control
                    type="file"
                    name="file"
                    size="sm"
                    onChange={(e) => {
                      setFieldValue("file", e.currentTarget.files[0]);
                    }}
                  />
                <Thumb file={values.file} />
                </Group> 
              </Row>
              <Row className="mt-4">
                <Col>
                  <Button variant="primary" type="submit">
                    Submit
                  </Button>
                </Col>
              </Row>
              </Form>           
            )}
          </Formik>
        </div>
      </div>
    );
  }
}

export default PromoCreatePage;
