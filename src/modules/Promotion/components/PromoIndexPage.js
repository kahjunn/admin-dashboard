import React, { Component } from 'react';
import { Table, Space, Modal, Button } from 'antd';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import moment from 'moment';
import apiCaller from '../../../tools/apiUtils';
import '../../../styles/MainContainer.scss';

class PromoIndexPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      promotion: []
    };
  }

  componentDidMount() {
    this.getPromotion ();
  }

  async getPromotion() {
    try {
      const res = await apiCaller.get('api/promotion/');
      const promotion = res.data.payload;
      this.setState({ promotion });
      console.log('Promotion Items', promotion)
    } catch (error) {
      console.log(error);
    }
  };

  deletepromotion(promotionId, promotionName) {
    const id = promotionId;
    const name = promotionName;
    Modal.confirm({
      title: `Are you sure to remove: ${name}`,
      okText: 'Yes, delete',
      okType: 'danger',
      cancelText: 'No',
      onOk: async() => {
        await apiCaller.delete(`api/promotion/delete/${id}`);
        Swal.fire({
          icon: 'success',
          title: 'Delete Success',
          text: `${name} deleted sucessfully!`
        });
        const res = await apiCaller.get('api/promotion/');
        const promotion = res.data.payload;
        this.setState({ promotion });
      }
    });
  };

  render(){
    const { promotion } = this.state;

    const columns = [
      {
        title: '#',
        dataIndex: 'id',
        width: '10%'
      },
      {
        title: 'Promotion Name',
        dataIndex: 'name',
        width: '25%'
      },
      {
        title: 'Created At',
        dataIndex: 'createdAt',
        width: '15%',
        render: createdAt => (
          moment(createdAt).format('LL')
        )
      },
      {
        title: 'Action',
        width: '20%',
        render: promotion  => (
          <span>
            <Space>
              <Link to={`/promotion/edit/${promotion.id}`}>
                <Button type="primary" shape="round">
                  Edit & View
                </Button>
              </Link>
              <Button type="danger" shape="round" onClick={() => this.deletepromotion(promotion.id, promotion.name)}>
                Delete
              </Button>
            </Space>
          </span>
        )
      }
    ]

    console.log('promotion Items', promotion);
    return(
      <div className="content">
        <div className="banner">
          <h1>Manage Promotions</h1> 
        </div>
        <div className="createButton">
          <Link to='/promotion/create'>
            <button type="button" className="btn btn-success">Add a Promotion</button>
          </Link>
        </div>
        <div className="indexTable">
          <Table columns={columns} dataSource={promotion} pagination={{ defaultPageSize: 12 }} />
        </div>
      </div>
    );
  }
}



export default PromoIndexPage;



