import React, { Component } from 'react';
import { Table, Space, Modal, Button } from 'antd';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import moment from 'moment';
import apiCaller from '../../../tools/apiUtils';
import '../../../styles/MainContainer.scss';

class AdminIndexPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      admin: []
    };
  }

  componentDidMount() {
    this.getAdmin ();
  }

  async getAdmin() {
    try {
      const res = await apiCaller.get('api/admin/');
      const admin = res.data.payload;
      this.setState({ admin });
      console.log('Admin Items', admin)
    } catch (error) {
      console.log(error);
    }
  };

  deleteAdmin(adminId, adminfName, adminlName) {
    const id = adminId;
    const fname = adminfName;
    const lname = adminlName;
    Modal.confirm({
      title: `Are you sure to remove: ${fname} ${lname}`,
      okText: 'Yes, delete',
      okType: 'danger',
      cancelText: 'No',
      onOk: async() => {
        await apiCaller.delete(`api/admin/delete/${id}`);
        Swal.fire({
          icon: 'success',
          title: 'Delete Success',
          text: `${fname} ${lname} deleted sucessfully!`
        });
        const res = await apiCaller.get('api/admin/');
        const admin = res.data.payload;
        this.setState({ admin });
      }
    });
  };

  render(){
    const { admin } = this.state;

    const columns = [
      {
        title: '#',
        dataIndex: 'id',
        width: '10%'
      },
      {
        title: 'Name',
        width: '20%',
        render: admin => (
          `${admin.firstName} ${admin.lastName}`
        )
      },
      {
        title: 'Email',
        dataIndex: 'email',
      width: '20%'
      },
      {
        title: 'Created At',
        dataIndex: 'createdAt',
        width: '20%',
        render: createdAt => (
          moment(createdAt).format('LL')
        )
      },
      {
        title: 'Action',
        width: '20%',
        render: admin  => (
          <span>
            <Space>
              <Link to={`/admin/edit/${admin.id}`}>
                <Button type="primary" shape="round">
                  Edit & View
                </Button>
              </Link>
              <Button type="danger" shape="round" onClick={() => this.deleteAdmin(admin.id, admin.firstName, admin.lastName)}>
                Delete
              </Button>
            </Space>
          </span>
        )
      }
    ]

    console.log('Admin Items', admin);
    return(
      <div className="content">
        <div className="banner">
          <h1>Manage Admin Users</h1> 
        </div>
        <div className="createButton">
          <Link to='/admin/create'>
            <button type="button" className="btn btn-success">Add an Admin User</button>
          </Link>
        </div>
        <div className="indexTable">
          <Table columns={columns} dataSource={admin} pagination={{ defaultPageSize: 12 }} />
        </div>
      </div>
    );
  }
}



export default AdminIndexPage;



