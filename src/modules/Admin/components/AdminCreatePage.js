import React, { Component } from 'react';
import { Button, Form, Col } from 'react-bootstrap';
import { Formik, Field, ErrorMessage } from 'formik';
import apiCaller from '../../../tools/apiUtils';
import Toast from '../../../tools/toaster';
import * as yup from 'yup';
import { Link, withRouter } from 'react-router-dom';
import '../../../styles/MainContainer.scss'
import '../styles/AdminForm.scss';

const { Group, Label, Control, Row } = Form;
const { Feedback } = Form.Control;

const adminSchema = yup.object().shape({
  firstName: yup.string().required("*Field is required"),
  lastName: yup.string().required("*Field is required"),
  email: yup.string().email("*Not a valid email").required("*Field is required"),
  password: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  phoneNo: yup.string().required("*Field is required"),
});

class AdminCreatePage extends Component {

  handleSubmit = async(values) => {
    try {
      const formData = { ...values };
      console.log('FormData to be submitted', formData);
      await apiCaller.post('api/admin/create', formData);
      Toast.fire({
        icon: 'success',
        title: `Admin User: ${formData.firstName} ${formData.lastName} , added successfully`
      })
      this.props.history.push('/admin');
    } catch (error) {
      console.log("Failed to submit");
      throw error;
    }
  };

  render() {
    return (
      <div className="admin-content">
        <div className="banner-create">
          <h1>Add New Admin User</h1>
        </div>
        <div className="backButton">
          <Link to="/admin">
            <Button variant="warning" size="sm">Back</Button>
          </Link>
        </div>

        <div className="create-form">
          <Formik
            initialValues={{
              firstName: "",
              lastName: "",
              email: "",
              password: "",
              phoneNo: "",
            }}
            validationSchema={adminSchema}
            onSubmit={(values, {setSubmitting, resetForm}) => {
              console.log("Form values: ", values);
              this.handleSubmit(values);
              setSubmitting(true);
              // resetForm();
              setSubmitting(false);
            }}
          >
            {({ touched, errors, values, handleChange, handleBlur, handleSubmit, isSubmitting }) => (
              <Form onSubmit={handleSubmit}>
              {console.log(values)}
              <Row>
                <Group as={Col} md="4">
                  <Label>First Name</Label>
                  <Control
                    type="text"
                    name="firstName"
                    size="sm"
                    placeholder="First Name"
                    onChange={handleChange}
                    value={values.firstName}
                    isInvalid={!!errors.firstName}
                  />
                  {touched.firstName && errors.firstName ? <Feedback type="invalid">{errors.firstName}</Feedback> : null}
                </Group>
                <Group as={Col} md="4">
                  <Label>Last Name</Label>
                  <Control
                    type="text"
                    name="lastName"
                    size="sm"
                    placeholder="Last Name"
                    onChange={handleChange}
                    value={values.lastName}
                    isInvalid={!!errors.lastName}
                  />
                  {touched.lastName && errors.lastName ? <Feedback type="invalid">{errors.lastName}</Feedback> : null}
                </Group>   
              </Row>
              <Row className="mt-2"> 
                <Group as={Col} md="4">
                  <Label>Email</Label>
                  <Control
                    type="email"
                    name="email"
                    size="sm"
                    placeholder="Email"
                    onChange={handleChange}
                    value={values.email}
                    isInvalid={!!errors.email}
                  />
                  {touched.email && errors.email ? <Feedback type="invalid">{errors.email}</Feedback> : null}
                </Group>   
                <Group as={Col} md="4">
                  <Label>Password</Label>
                  <Control
                    type="password"
                    name="password"
                    size="sm"
                    placeholder="Password"
                    onChange={handleChange}
                    value={values.password}
                    isInvalid={!!errors.password}
                  />
                  {touched.password && errors.password ? <Feedback type="invalid">{errors.password}</Feedback> : null}
                </Group>
              </Row>
              <Row className="mt-4">
                <Group as={Col} md="3">
                  <Label>Phone Number</Label>
                  <Control
                    type="text"
                    name="phoneNo"
                    size="sm"
                    placeholder="Phone Number"
                    onChange={handleChange}
                    value={values.phoneNo}
                    isInvalid={!!errors.phoneNo}
                  />
                  {touched.phoneNo && errors.phoneNo ? <Feedback type="invalid">{errors.phoneNo}</Feedback> : null}
                </Group>
              </Row>
              <Row className="mt-4">
                <Col>
                  <Button variant="primary" type="submit">
                    Submit
                  </Button>
                </Col>
              </Row>
              </Form>           
            )}
          </Formik>
        </div>
      </div>
    );
  }
}

export default AdminCreatePage;
