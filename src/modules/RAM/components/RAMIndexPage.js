import React, { Component } from 'react';
import { Table, Space, Modal, Button } from 'antd';
import { Link } from 'react-router-dom';
import moment from 'moment';
import Swal from 'sweetalert2';
import apiCaller from '../../../tools/apiUtils';
import '../../../styles/MainContainer.scss';

class RAMIndexPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      RAM: []
    };
  }

  componentDidMount() {
    this.getRAM();
  }

  async getRAM() {
    try {
      const res = await apiCaller.get('api/ram/');
      const RAM = res.data.payload;
      this.setState({ RAM });
      console.log('CPU Items', RAM)
    } catch (error) {
      console.log(error);
    }
  };

  deleteRAM(ramId, ramName) {
    const id = ramId;
    const name = ramName;
    Modal.confirm({
      title: `Are you sure to remove: ${name}`,
      okText: 'Yes, delete',
      okType: 'danger',
      cancelText: 'No',
      onOk: async() => {
        await apiCaller.delete(`api/ram/delete/${id}`);
        Swal.fire({
          icon: 'success',
          title: 'Delete Success',
          text: `${name} deleted sucessfully!`
        });
        const res = await apiCaller.get('api/ram/');
        const RAM = res.data.payload;
        this.setState({ RAM });
      }
    });
  };

  render(){
    const { RAM } = this.state;

    const columns = [
      {
        title: '#',
        dataIndex: 'id',
        width: '5%'
      },
      {
        title: 'Name',
        dataIndex: 'name',
        width: '35%'
      },
      {
        title: 'Brand',
        dataIndex: 'brand',
        width: '13%'
      },
      {
        title: 'Created At',
        dataIndex: 'createdAt',
        width: '15%',
        render: createdAt => (
          moment(createdAt).format('LL')
        )
      },
      {
        title: 'Price',
        dataIndex: 'price',
        width: '15%',
        render: price => (
          `RM ${price}`
        )
      },
      {
        title: 'Action',
        render: RAM => (
          <span>
            <Space>
              <Link to={`/ram/edit/${RAM.id}`}>
                <Button type="primary" shape="round">
                  Edit & View
                </Button>
              </Link>
              <Button type="danger" shape="round" onClick={() => this.deleteRAM(RAM.id, RAM.name)}>
                Delete
              </Button>
            </Space>
          </span>
        )
      }
    ]

    console.log('CPU Items', RAM);
    return(
      <div className="content">
        <div className="banner">
          <h1>Manage PC Parts: Memory (RAM)</h1> 
        </div>
        <div className="createButton">
          <Link to='/ram/create'>
            <button type="button" className="btn btn-success">Add a RAM</button>
          </Link>
        </div>
        <div className="indexTable">
          <Table columns={columns} dataSource={RAM} pagination={{ defaultPageSize: 12 }} />
        </div>
      </div>
    );
  }
}



export default RAMIndexPage;



