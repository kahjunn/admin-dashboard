import React, { Component } from 'react';
import { Button, Form, Col } from 'react-bootstrap';
import { Formik, Field, ErrorMessage } from 'formik';
import apiCaller from '../../../tools/apiUtils';
import Toast from '../../../tools/toaster';
import * as yup from 'yup';
import { Link, withRouter } from 'react-router-dom';
import '../../../styles/MainContainer.scss'
import '../styles/RAMForm.scss';

const { Group, Label, Control, Row } = Form;
const { Feedback } = Form.Control;

const ramSchema = yup.object().shape({
  name: yup.string().required("*Field is required"),
  brand: yup.string().required("*Field is required"),
  model: yup.string().required("*Field is required"),
  memoryType: yup.string().required("*Selection is required"),
  memorySpeed: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  modules: yup.string().required("*Selection is required"),
  color: yup.string().required("*Field is required"),
  heatSpreader: yup.string().required("*Selection is required"),
  rgb: yup.string().required("*Selection is required"),
  price: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  detail1: yup.string().required("*Field is required"),
  detail2: yup.string().required("*Field is required"),
  detail3: yup.string().required("*Field is required"),
});

class RAMCreatePage extends Component {

  handleSubmit = async(values) => {
    try {
      const formData = { ...values };
      console.log('FormData to be submitted', formData);
      await apiCaller.post('api/ram/create', formData);
      Toast.fire({
        icon: 'success',
        title: `RAM: ${formData.name} , added successfully`
      })
      this.props.history.push('/ram');
    } catch (error) {
      console.log("Failed to submit");
      throw error;
    }
  };

  render() {
    return (
      <div className="ram-content">
        <div className="banner-create">
          <h1>Add New Memory (RAM)</h1>
        </div>
        <div className="backButton">
          <Link to="/ram">
            <Button variant="warning" size="sm">Back</Button>
          </Link>
        </div>

        <div className="create-form">
          <Formik
            initialValues={{
              name: "",
              brand: "",
              model: "",
              memoryType: "",
              memorySpeed: "",
              modules: "",
              color: "",
              heatSpreader: "",
              rgb: "",
              price: "",
              detail1: "",
              detail2: "",
              detail3: ""
            }}
            validationSchema={ramSchema}
            onSubmit={(values, {setSubmitting, resetForm}) => {
              console.log("Form values: ", values);
              this.handleSubmit(values);
              setSubmitting(true);
              // resetForm();
              setSubmitting(false);
            }}
          >
            {({ touched, errors, values, handleChange, handleBlur, handleSubmit, isSubmitting }) => (
              <Form onSubmit={handleSubmit}>
              {console.log(values)}
              <Row>
                <Group as={Col} md="4">
                  <Label>Name</Label>
                  <Control
                    type="text"
                    name="name"
                    size="sm"
                    placeholder="Name"
                    onChange={handleChange}
                    value={values.name}
                    isInvalid={!!errors.name}
                  />
                  {touched.name && errors.name ? <Feedback type="invalid">{errors.name}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Brand</Label>
                  <Control
                    type="text"
                    name="brand"
                    size="sm"
                    placeholder="Brand"
                    onChange={handleChange}
                    value={values.brand}
                    isInvalid={!!errors.brand}
                  />
                  {touched.brand && errors.brand ? <Feedback type="invalid">{errors.brand}</Feedback> : null}
                </Group>   
                <Group as={Col} md="4">
                  <Label>Model</Label>
                  <Control
                    type="text"
                    name="model"
                    size="sm"
                    placeholder="Model"
                    onChange={handleChange}
                    value={values.model}
                    isInvalid={!!errors.model}
                  />
                  {touched.model && errors.model ? <Feedback type="invalid">{errors.model}</Feedback> : null}
                </Group>   
              </Row>
              <Row className="mt-2"> 
                <Group as={Col} md="2">
                  <Label>Memory Type</Label>
                  <Control
                    as="select"
                    name="memoryType"
                    size="sm"
                    onChange={handleChange}
                    isInvalid={!!errors.memoryType}
                  >
                    <option selected disabled>Choose...</option>
                    <option value="DDR4">DDR4</option>
                    <option value="DDR3">DDR3</option>
                    <option value="DDR2">DDR2</option>
                    <option value="DDR">DDR</option>
                  </Control>
                  {touched.memoryType && errors.memoryType ? <Feedback type="invalid">{errors.memoryType}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Memory Speed (MHz)</Label>
                  <Control
                    type="text"
                    name="memorySpeed"
                    size="sm"
                    placeholder="Memory Speed (MHz)"
                    onChange={handleChange}
                    value={values.memorySpeed}
                    isInvalid={!!errors.memorySpeed}
                  />
                  {touched.memorySpeed && errors.memorySpeed ? <Feedback type="invalid">{errors.memorySpeed}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Modules (No.)</Label>
                  <Control
                    as="select"
                    name="modules"
                    size="sm"
                    onChange={handleChange}
                    isInvalid={!!errors.modules}
                  >
                    <option selected disabled>Choose...</option>
                    <option value="1 x 4GB">1 x 4GB</option>
                    <option value="2 x 4GB">2 x 4GB</option>
                    <option value="4 x 4GB">4 x 4GB</option>
                    <option value="1 x 8GB">1 x 8GB</option>
                    <option value="2 x 8GB">2 x 8GB</option>
                    <option value="4 x 8GB">4 x 8GB</option>
                    <option value="1 x 16GB">1 x 16GB</option>
                    <option value="2 x 16GB">2 x 16GB</option>
                    <option value="4 x 16GB">4 x 16GB</option>
                    <option value="1 x 32GB">1 x 32GB</option>
                    <option value="2 x 32B">2 x 32GB</option>
                    <option value="4 x 32GB">4 x 32GB</option>
                  </Control>
                  {touched.modules && errors.modules ? <Feedback type="invalid">{errors.modules}</Feedback> : null}
                </Group>
                <Group as={Col} md="4">
                  <Label>Color</Label>
                  <Control
                    type="text"
                    name="color"
                    size="sm"
                    placeholder="Color"
                    onChange={handleChange}
                    value={values.color}
                    isInvalid={!!errors.color}
                  />
                  {touched.color && errors.color ? <Feedback type="invalid">{errors.color}</Feedback> : null}
                </Group>
              </Row >
              <Row className="mt-2">
                <Group as={Col} md="2">
                  <Label>Heat Spreader</Label>
                  <Control
                    as="select"
                    name="heatSpreader"
                    size="sm"
                    onChange={handleChange}
                    isInvalid={!!errors.heatSpreader}
                  >
                    <option selected disabled>Choose...</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                  </Control>
                  {touched.heatSpreader && errors.heatSpreader ? <Feedback type="invalid">{errors.heatSpreader}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>RGB</Label>
                  <Control
                    as="select"
                    name="rgb"
                    size="sm"
                    onChange={handleChange}
                    isInvalid={!!errors.rgb}
                  >
                    <option selected disabled>Choose...</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                  </Control>
                  {touched.rgb && errors.rgb ? <Feedback type="invalid">{errors.rgb}</Feedback> : null}
                </Group>
                <Group as={Col} md="3">
                  <Label>Price (RM)</Label>
                  <Control
                    type="text"
                    name="price"
                    size="sm"
                    placeholder="Price"
                    onChange={handleChange}
                    value={values.price}
                    isInvalid={!!errors.price}
                  />
                  {touched.price && errors.price ? <Feedback type="invalid">{errors.price}</Feedback> : null}
                </Group>
              </Row>
              <Row>
                <Col md="5">
                  <Group>
                    <Label>Details 1</Label>
                    <Control
                      type="text"
                      name="detail1"
                      size="sm"
                      placeholder="Details 1"
                      onChange={handleChange}
                      value={values.detail1}
                      isInvalid={!!errors.detail1}
                    />
                    {touched.detail1 && errors.detail1 ? <Feedback type="invalid">{errors.detail1}</Feedback> : null}
                  </Group>
                  <Group>
                    <Label>Details 2</Label>
                    <Control
                      type="text"
                      name="detail2"
                      size="sm"
                      placeholder="Details 2"
                      onChange={handleChange}
                      value={values.detail2}
                      isInvalid={!!errors.detail2}
                    />
                    {touched.detail2 && errors.detail2 ? <Feedback type="invalid">{errors.detail2}</Feedback> : null}
                  </Group>
                  <Group>
                    <Label>Details 3</Label>
                    <Control
                      type="text"
                      name="detail3"
                      size="sm"
                      placeholder="Details 3"
                      onChange={handleChange}
                      value={values.detail3}
                      isInvalid={!!errors.detail3}
                    />
                    {touched.detail3 && errors.detail3 ? <Feedback type="invalid">{errors.detail3}</Feedback> : null}
                  </Group>
                </Col>
              </Row>
              <Row className="mt-4">
                <Col>
                  <Button variant="primary" type="submit">
                    Submit
                  </Button>
                </Col>
              </Row>
              </Form>           
            )}
          </Formik>
        </div>
      </div>
    );
  }
}

export default RAMCreatePage;
