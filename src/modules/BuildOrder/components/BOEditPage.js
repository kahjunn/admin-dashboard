import React, { Component } from 'react';
import { Button, Form, Row, Col, Table } from 'react-bootstrap';
import { Formik, Field, ErrorMessage } from 'formik';
import { Modal } from 'antd';
import apiCaller from '../../../tools/apiUtils';
import moment from 'moment';
import Toast from '../../../tools/toaster';
import * as yup from 'yup';
import { Link, withRouter } from 'react-router-dom';
import '../../../styles/MainContainer.scss'
import '../styles/BOForm.scss';

const { Group, Label, Control } = Form;

class BOEditPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      boId: props.match.params.id,
      bo: {
        cpu: {},
        gpu: {},
        motherboard: {},
        ram: {},
        storage: {},
        psu: {},
        cooler: {},
        case: {},
        customer: {}
      }
    };
  }

  componentDidMount() {
    this.getBO();
  }

  async getBO() {
    const res = await apiCaller.get(`/api/buildOrder/${this.state.boId}`);
    const bo = res.data.payload;
    this.setState({ bo });
  }

  handleSubmit = async(values) => {
    try {
      const status = { ...values };
      console.log('to be submitted: ', values);
      Modal.confirm({
        title: `Are you sure to update order status for: #BO${this.state.bo.orderNo}`,
        okText: 'Yes, update',
        okType: 'primary',
        cancelText: 'No',
        onOk: async() => {
          console.log('Fireeeeeeeeeee');
          await apiCaller.put(`api/buildOrder/updateStatus/${this.state.boId}`, status);
          this.props.history.push('/bo');
        }
      })
    } catch (error) {
      throw error;
    }
  }
  
  render() {
    const { bo } = this.state;
    console.log('BuildOrder: ', bo);
    return (
      <div className="bo-content">
        <Row>
          <Col>
            <div className="banner-create">
              <h1>Manage Build Order: #BO{bo.orderNo}</h1>
            </div>
          </Col>
        </Row>
        <Row>
          <Col>        
            <div className="backButton">
              <Link to="/bo">
                <Button variant="warning" size="sm">Back</Button>
              </Link>
            </div>
          </Col>
        </Row> 

        
        <div className="bo-table">
        <h3 className="bo-banner">Build Order Details</h3>
          <Table striped bordered hover variant="dark">
            <thead>
              <tr>
                <th width="1%">#</th>
                <th width="8%">Type</th>
                <th width="35%">Selected PC Parts Name</th>
                <th width="15%">Price / unit (RM)</th>
                <th width="15%">Total (RM)</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>CPU</td>
                <td>{bo.cpu.name}</td>
                <td>RM {bo.cpu.price}</td>
                <td>RM {bo.cpu.price}</td> 
              </tr>
              <tr>
                <td>2</td>
                <td>GPU</td>
                <td>{bo.gpu.name}</td>
                <td>RM {bo.gpu.price}</td>
                <td>RM {bo.gpu.price}</td> 
              </tr>
              <tr>
                <td>3</td>
                <td>Motherboard</td>
                <td>{bo.motherboard.name}</td>
                <td>RM {bo.motherboard.price}</td>
                <td>RM {bo.motherboard.price}</td> 
              </tr>
              <tr>
                <td>4</td>
                <td>RAM</td>
                <td>{bo.ram.name}</td>
                <td>RM {bo.ram.price}</td>
                <td>RM {bo.ram.price}</td> 
              </tr>
              <tr>
                <td>5</td>
                <td>Storage</td>
                <td>{bo.storage.name}</td>
                <td>RM {bo.storage.price}</td>
                <td>RM {bo.storage.price}</td> 
              </tr>
              <tr>
                <td>6</td>
                <td>PSU</td>
                <td>{bo.psu.name}</td>
                <td>RM {bo.psu.price}</td>
                <td>RM {bo.psu.price}</td> 
              </tr>
              <tr>
                <td>7</td>
                <td>CPU Cooler</td>
                <td>{bo.cooler.name}</td>
                <td>RM {bo.cooler.price}</td>
                <td>RM {bo.cooler.price}</td> 
              </tr>
              <tr>
                <td>8</td>
                <td>PC Case</td>
                <td>{bo.case.name}</td>
                <td>RM {bo.case.price}</td>
                <td>RM {bo.case.price}</td> 
              </tr>
              <tr>
                <td className="text-right grandTotal" colSpan="4">Grand Total: </td> 
                <td className="grandTotal">RM {bo.price}</td> 
              </tr> 
            </tbody> 
          </Table>
        </div>

        <h3 className="customer-banner">Customer Details</h3>
        <div className="customer-table">
          <Table striped bordered hover variant="success">
            <tr>
              <th width="1%">Name</th>
              <td width="20%">{bo.customer.firstName} {bo.customer.lastName}</td>
            </tr>
            <tr>
              <th>Email</th>
              <td>{bo.customer.email}</td>
            </tr>
            <tr>
              <th>Phone No</th>
              <td>{bo.customer.phoneNo}</td>
            </tr>
            <tr>
              <th>Address 1</th>
              <td>{bo.customer.address1}</td>
            </tr>
            <tr>
              <th>Address 2</th>
              <td>{bo.customer.address2}</td>
            </tr>
            <tr>
              <th>City</th>
              <td>{bo.customer.city}</td>
            </tr>
            <tr>
              <th>State</th>
              <td>{bo.customer.state}</td>
            </tr>
            <tr>
              <th>Zip Code</th>
              <td>{bo.customer.zipCode}</td>
            </tr>
            <tr>
              <th>Created At</th>
              <td>{moment(bo.createdAt).format('LLL')}</td>
            </tr>
          </Table>
        </div>
        
        <div className="bo-update"> 
          <Formik
            initialValues={bo}
            enableReinitialize
            onSubmit={(values, {setSubmitting}) => {
              console.log("Button values: ", values);
              this.handleSubmit(values);
              setSubmitting(true); 
              setSubmitting(false);
            }}
          >
            {({ touched, errors, values, handleChange, handleBlur, handleSubmit, isSubmitting }) => (
              <Form onSubmit={handleSubmit}>
                <Row>
                  <Col md={{ span: 4 }}> 
                    <Group>
                      <Label className="label">Status</Label>
                      {console.log(values)}
                      <Control 
                        as="select"
                        name="status"
                        size="sm"
                        onChange={handleChange}
                        value={values.status}
                        >
                        <option disabled>Choose...</option>
                        <option value="Pending Payment">Pending Payment</option>
                        <option value="Processing">Processing</option>
                        <option value="Build Complete">Build Complete</option>
                      </Control>
                    </Group>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Button variant="primary" type="submit">
                      Update  
                    </Button> 
                  </Col>
                </Row> 
              </Form>
            )}
          </Formik>
        </div>
      </div>
    )
  }
}

export default BOEditPage;
