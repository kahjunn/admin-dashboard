import React, { Component } from 'react';
import { Table, Space, Modal, Button, Tag } from 'antd';
import { Link } from 'react-router-dom';
import moment from 'moment';
import apiCaller from '../../../tools/apiUtils';
import '../../../styles/MainContainer.scss';

class BOIndexPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buildOrders: []
    };
  }

  componentDidMount() {
    this.getBuildOrders();
  }

  async getBuildOrders() {
    try {
      const res = await apiCaller.get('api/buildOrder/');
      const buildOrders = res.data.payload;
      this.setState({ buildOrders });
    } catch (error) {
      console.log(error);
    }
  };

  render(){
    const { buildOrders } = this.state;
    console.log('BO Items', buildOrders)
    
    const columns = [
      {
        title: '#',
        dataIndex: 'id',
        width: '10%'
      },
      {
        title: 'Customer Name',
        dataIndex: 'customer',
        width: '20%',
        render: c => (
          <div>
            {c.firstName} {c.lastName}
          </div>
        )
      },
      {
        title: 'Build Order No',
        dataIndex: 'orderNo',
        width: '10%',
        render: order => (
          <div>
            #BO{order}
          </div>
        )
      },
      {
        title: 'Status',
        dataIndex: 'status',
        width: '20%',
        align: 'center',
        render: status => {
          switch (status) {
            case 'Pending Payment':
              return <Tag color="orange">Pending Payment</Tag>;
            case 'Processing':
              return <Tag color="blue">Processing</Tag>;
            case 'Build Complete':
              return <Tag color="green">Build Complete</Tag>;
            case 'Order Cancelled':
              return <Tag color="red">Order Cancelled</Tag>;
            default:
              return <span>-</span>;
          } 
        }
      },
      {
        title: 'Created At',
        dataIndex: 'createdAt',
        width: '15%',
        render: createdAt => (
          moment(createdAt).format('LL')
        )
      },
      {
        title: 'Price',
        dataIndex: 'price',
        width: '15%',
        render: price => (
          `RM ${price}`
        )
      },
      {
        title: 'Action',
        width: '20%',
        render: buildOrders => (
          <Space size="middle">
            <Link to={`/bo/edit/${buildOrders.id}`}>
              <Button type="primary" shape="round">
                 Edit & View
              </Button>
            </Link>
          </Space>
        )
      }
    ]

    return(
      <div className="content">
        <div className="banner">
          <h1>Manage PC Parts: Build Orders</h1> 
        </div>
        <div className="indexTable">
          <Table columns={columns} dataSource={buildOrders} pagination={{ defaultPageSize: 10 }} />
        </div>
      </div>
    );
  }
}



export default BOIndexPage;



