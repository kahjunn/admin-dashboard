import React, { Component } from 'react';
import { Button, Form, Col } from 'react-bootstrap';
import { Formik, Field, ErrorMessage } from 'formik';
import apiCaller from '../../../tools/apiUtils';
import Toast from '../../../tools/toaster';
import * as yup from 'yup';
import { Link, withRouter } from 'react-router-dom';
import '../../../styles/MainContainer.scss'
import '../styles/StorageForm.scss';

const { Group, Label, Control, Row } = Form;
const { Feedback } = Form.Control;

const storageSchema = yup.object().shape({
  name: yup.string().required("*Field is required"),
  brand: yup.string().required("*Field is required"),
  model: yup.string().required("*Field is required"),
  storageType: yup.string().required("*Selection is required"),
  rpmSpeed: yup.string().matches(/[0-9]/, "*Numbers only"),
  volume: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  formFactor: yup.string().required("*Selection is required"),
  storageInterface: yup.string().required("*Selection is required"),
  nvme: yup.string().required("*Selection is required"),
  price: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  detail1: yup.string().required("*Field is required"),
  detail2: yup.string().required("*Field is required"),
  detail3: yup.string().required("*Field is required"),
});

class StorageCreatePage extends Component {

  handleSubmit = async(values) => {
    try {
      const formData = { ...values };
      console.log('FormData to be submitted', formData);
      await apiCaller.post('api/storage/create', formData);
      Toast.fire({
        icon: 'success',
        title: `Added Successfully: ${formData.name}`
      })
      this.props.history.push('/storage');
    } catch (error) {
      console.log("Failed to submit");
      throw error;
    }
  };

  render() {
    return (
      <div className="storage-content">
        <div className="banner-create">
          <h1>Add New Storage</h1>
        </div>
        <div className="backButton">
          <Link to="/storage">
            <Button variant="warning" size="sm">Back</Button>
          </Link>
        </div>

        <div className="create-form">
          <Formik
            initialValues={{
              name: "",
              brand: "",
              model: "",
              type: "",
              rpmSpeed: "",
              volume: "",
              formFactor: "",
              storageInterface: "",
              nvme: "",
              price: "",
              detail1: "",
              detail2: "",
              detail3: ""
            }}
            validationSchema={storageSchema}
            onSubmit={(values, {setSubmitting, resetForm}) => {
              console.log("Form values: ", values);
              this.handleSubmit(values);
              setSubmitting(true);
              // resetForm();
              setSubmitting(false);
            }}
          >
            {({ touched, errors, values, handleChange, handleBlur, handleSubmit, isSubmitting }) => (
              <Form onSubmit={handleSubmit}>
              {console.log(values)}
              <Row>
                <Group as={Col} md="4">
                  <Label>Name</Label>
                  <Control
                    type="text"
                    name="name"
                    size="sm"
                    placeholder="Name"
                    onChange={handleChange}
                    value={values.name}
                    isInvalid={!!errors.name}
                  />
                  {touched.name && errors.name ? <Feedback type="invalid">{errors.name}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Brand</Label>
                  <Control
                    type="text"
                    name="brand"
                    size="sm"
                    placeholder="Brand"
                    onChange={handleChange}
                    value={values.brand}
                    isInvalid={!!errors.brand}
                  />
                  {touched.brand && errors.brand ? <Feedback type="invalid">{errors.brand}</Feedback> : null}
                </Group>   
                <Group as={Col} md="4">
                  <Label>Model</Label>
                  <Control
                    type="text"
                    name="model"
                    size="sm"
                    placeholder="Model"
                    onChange={handleChange}
                    value={values.model}
                    isInvalid={!!errors.model}
                  />
                  {touched.model && errors.model ? <Feedback type="invalid">{errors.model}</Feedback> : null}
                </Group>   
              </Row>
              <Row className="mt-2"> 
                <Group as={Col} md="3">
                  <Label>Type</Label>
                  <Control
                    as="select"
                    name="storageType"
                    size="sm"
                    onChange={handleChange}
                    isInvalid={!!errors.storageType}
                  >
                    <option selected disabled>Choose...</option>
                    <option value="SSD">SSD</option>
                    <option value="HDD">HDD</option>
                  </Control>
                  {touched.storageType && errors.storageType ? <Feedback type="invalid">{errors.storageType}</Feedback> : null}
                </Group>
                <Group as={Col} md="3">
                  <Label>RPM Speed (HDD Only)</Label>
                  <Control
                    type="text"
                    name="rpmSpeed"
                    size="sm"
                    placeholder="RPM Speed (HDD Only)"
                    onChange={handleChange}
                    value={values.rpmSpeed}
                    isInvalid={!!errors.rpmSpeed}
                  />
                  {touched.rpmSpeed && errors.rpmSpeed ? <Feedback type="invalid">{errors.rpmSpeed}</Feedback> : null}
                </Group>
                <Group as={Col} md="3">
                  <Label>Volume (GB)</Label>
                  <Control
                    type="text"
                    name="volume"
                    size="sm"
                    placeholder="Volume (GB)"
                    onChange={handleChange}
                    value={values.volume}
                    isInvalid={!!errors.volume}
                  />
                  {touched.volume && errors.volume ? <Feedback type="invalid">{errors.volume}</Feedback> : null}
                </Group>
              </Row >
              <Row className="mt-2">
                <Group as={Col} md="3">
                  <Label>Form Factor</Label>
                  <Control
                    as="select"
                    name="formFactor"
                    size="sm"
                    onChange={handleChange}
                    isInvalid={!!errors.formFactor}
                  >
                    <option selected disabled>Choose...</option>
                    <option value='1.8"'>1.8"</option>
                    <option value='2.5"'>2.5"</option>
                    <option value='3.5"'>3.5"</option>
                    <option value='M.2 22110'>M.2 22110</option>
                    <option value='M.2 2242'>M.2 2242</option>
                    <option value='M.2 2260'>M.2 2260</option>
                    <option value='M.2 2280'>M.2 2280</option>
                    <option value='mSATA'>mSATA</option>
                    <option value='PCI-E'>PCI-E</option> 
                  </Control>
                  {touched.formFactor && errors.formFactor ? <Feedback type="invalid">{errors.formFactor}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Interface</Label>
                  <Control
                    as="select"
                    name="storageInterface"
                    size="sm"
                    onChange={handleChange}
                    isInvalid={!!errors.storageInterface}
                  >
                    <option selected disabled>Choose...</option>
                    <option>SATA 6 Gb/s</option>
                    <option>SATA 3 Gb/s</option>
                    <option>SATA 1.5 Gb/s</option>
                    <option>Micro SATA 6 Gb/s</option>
                    <option>Micro SATA 3 Gb/s</option>
                    <option>PCIe x16</option>
                    <option>PCIe x8</option>
                    <option>PCIe x4</option>
                    <option>PCIe x2</option>
                    <option>PCIe x1</option>
                    <option>mSATA</option>
                    <option>M.2 (B+M)</option>
                    <option>M.2 (M)</option>
                    <option>U.2</option>
                  </Control>
                  {touched.storageInterface && errors.storageInterface ? <Feedback type="invalid">{errors.storageInterface}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>NVME</Label>
                  <Control
                    as="select"
                    name="nvme"
                    size="sm"
                    onChange={handleChange}
                    isInvalid={!!errors.nvme}
                  >
                    <option selected disabled>Choose...</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                  </Control>
                  {touched.nvme && errors.nvme ? <Feedback type="invalid">{errors.nvme}</Feedback> : null}
                </Group>
              </Row>
              <Row className="mt-2">
                <Group as={Col} md="3">
                  <Label>Price (RM)</Label>
                  <Control
                    type="text"
                    name="price"
                    size="sm"
                    placeholder="Price"
                    onChange={handleChange}
                    value={values.price}
                    isInvalid={!!errors.price}
                  />
                  {touched.price && errors.price ? <Feedback type="invalid">{errors.price}</Feedback> : null}
                </Group>
              </Row>
              <Row>
                <Col md="5">
                  <Group>
                    <Label>Details 1</Label>
                    <Control
                      type="text"
                      name="detail1"
                      size="sm"
                      placeholder="Details 1"
                      onChange={handleChange}
                      value={values.detail1}
                      isInvalid={!!errors.detail1}
                    />
                    {touched.detail1 && errors.detail1 ? <Feedback type="invalid">{errors.detail1}</Feedback> : null}
                  </Group>
                  <Group>
                    <Label>Details 2</Label>
                    <Control
                      type="text"
                      name="detail2"
                      size="sm"
                      placeholder="Details 2"
                      onChange={handleChange}
                      value={values.detail2}
                      isInvalid={!!errors.detail2}
                    />
                    {touched.detail2 && errors.detail2 ? <Feedback type="invalid">{errors.detail2}</Feedback> : null}
                  </Group>
                  <Group>
                    <Label>Details 3</Label>
                    <Control
                      type="text"
                      name="detail3"
                      size="sm"
                      placeholder="Details 3"
                      onChange={handleChange}
                      value={values.detail3}
                      isInvalid={!!errors.detail3}
                    />
                    {touched.detail3 && errors.detail3 ? <Feedback type="invalid">{errors.detail3}</Feedback> : null}
                  </Group>
                </Col>
              </Row>
              <Row className="mt-4">
                <Col>
                  <Button variant="primary" type="submit">
                    Submit
                  </Button>
                </Col>
              </Row>
              </Form>           
            )}
          </Formik>
        </div>
      </div>
    );
  }
}

export default StorageCreatePage;
