import React, { Component } from 'react';
import { Table, Space, Modal, Button } from 'antd';
import { Link } from 'react-router-dom';
import moment from 'moment';
import Swal from 'sweetalert2';
import apiCaller from '../../../tools/apiUtils';
import '../../../styles/MainContainer.scss';

class StorageIndexPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      storage: []
    };
  }

  componentDidMount() {
    this.getStorage();
  }

  async getStorage() {
    try {
      const res = await apiCaller.get('api/storage/');
      const storage = res.data.payload;
      this.setState({ storage });
      console.log('storage Items', storage)
    } catch (error) {
      console.log(error);
    }
  };

  deleteStorage(storageId, storageName) {
    const id = storageId;
    const name = storageName;
    Modal.confirm({
      title: `Are you sure to remove: ${name}`,
      okText: 'Yes, delete',
      okType: 'danger',
      cancelText: 'No',
      onOk: async() => {
        await apiCaller.delete(`api/storage/delete/${id}`);
        Swal.fire({
          icon: 'success',
          title: 'Delete Success',
          text: `${name} deleted sucessfully!`
        });
        const res = await apiCaller.get('api/storage/');
        const storage = res.data.payload;
        this.setState({ storage });
      }
    });
  };

  render(){
    const { storage } = this.state;

    const columns = [
      {
        title: '#',
        dataIndex: 'id',
        width: '5%'
      },
      {
        title: 'Name',
        dataIndex: 'name',
        width: '25%'
      },
      {
        title: 'Brand',
        dataIndex: 'brand',
        width: '10%'
      },
      {
        title: 'Created At',
        dataIndex: 'createdAt',
        width: '13%',
        render: createdAt => (
          moment(createdAt).format('LL')
        )
      },
      {
        title: 'Price',
        dataIndex: 'price',
        width: '12%',
        render: price => (
          `RM ${price}`
        )
      },
      {
        title: 'Action',
        width: '20%',
        render: storage => (
          <span>
            <Space>
              <Link to={`/storage/edit/${storage.id}`}>
                <Button type="primary" shape="round">
                  Edit & View
                </Button>
              </Link>
              <Button type="danger" shape="round" onClick={() => this.deleteStorage(storage.id, storage.name)}>
                Delete
              </Button>
            </Space>
          </span>
        )
      }
    ]

    console.log('storage Items', storage);
    return(
      <div className="content">
        <div className="banner">
          <h1>Manage PC Parts: Storage</h1> 
        </div>
        <div className="createButton">
          <Link to='/storage/create'>
            <button type="button" className="btn btn-success">Add a Storage</button>
          </Link>
        </div>
        <div className="indexTable">
          <Table columns={columns} dataSource={storage} pagination={{ defaultPageSize: 12 }} />
        </div>
      </div>
    );
  }
}



export default StorageIndexPage;



