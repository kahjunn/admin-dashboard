import React, { Component } from 'react';
import { Button, Form, Col } from 'react-bootstrap';
import { Formik, Field, ErrorMessage } from 'formik';
import apiCaller from '../../../tools/apiUtils';
import Toast from '../../../tools/toaster';
import * as yup from 'yup';
import { Link, withRouter } from 'react-router-dom';
import '../../../styles/MainContainer.scss'
import '../styles/PCCaseForm.scss';

const { Group, Label, Control, Row } = Form;
const { Feedback } = Form.Control;

const casingSchema = yup.object().shape({
  name: yup.string().required("*Field is required"),
  brand: yup.string().required("*Field is required"),
  model: yup.string().required("*Field is required"),
  formFactor: yup.string().required("*Selection is required"),
  sidePanel: yup.string().required("*Selection is required"),
  color: yup.string().required("*Field is required"),
  shroud: yup.string().required("*Selection is required"),
  maxGPULength: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  bay2: yup.string().required("*Field is required"),
  bay3: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  supportedFormFactor: yup.string().required("*Field is required"),
  length: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  width: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  height: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  weight: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  price: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  detail1: yup.string().required("*Field is required"),
  detail2: yup.string().required("*Field is required"),
  detail3: yup.string().required("*Field is required"),
});

class CasingEditPage extends Component {
  constructor(props) {
    super(props); 
    this.state = {
      casingId: props.match.params.id,
      casing: {}
    };
  }

  componentDidMount() {
    this.getCasing();
  }

  async getCasing() {
    const res = await apiCaller.get(`/api/case/${this.state.casingId}`);
    const casing = res.data.payload;
    this.setState({ casing });
  }

  handleSubmit = async(values) => {
    try {
      const formData = { ...values };
      console.log('FormData to be submitted', formData);
      await apiCaller.put(`api/case/update/${this.state.casingId}`, formData);
      const res = await apiCaller.get(`/api/case/${this.state.casingId}`);
      const casing = res.data.payload;
      this.setState({ casing });
      Toast.fire({
        icon: 'success',
        title: `Update Success: ${this.state.casing.name}`
      })
      this.props.history.push('/casing');
    } catch (error) {
      console.log("Failed to submit");
      throw error;
    }
  };

  render() {
    const { casing } = this.state;
    return (
      <div className="casing-content">
        <div className="banner-create">
          <h1>Edit PC Case: {casing.name}</h1>
        </div>
        <div className="backButton">
          <Link to="/casing">
            <Button variant="warning" size="sm">Back</Button>
          </Link>
        </div>

        <div className="create-form">
          <Formik
            initialValues={casing}
            enableReinitialize
            validationSchema={casingSchema}
            onSubmit={(values, {setSubmitting, resetForm}) => {
              console.log("Form values: ", values);
              this.handleSubmit(values);
              setSubmitting(true);
              // resetForm();
              setSubmitting(false);
            }}
          >
            {({ touched, errors, values, handleChange, handleBlur, handleSubmit, isSubmitting }) => (
              <Form onSubmit={handleSubmit}>
              {console.log(values)}
              <Row>
                <Group as={Col} md="4">
                  <Label>Name</Label>
                  <Control
                    type="text"
                    name="name"
                    size="sm"
                    placeholder="Name"
                    onChange={handleChange}
                    value={values.name}
                    isInvalid={!!errors.name}
                  />
                  {touched.name && errors.name ? <Feedback type="invalid">{errors.name}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Brand</Label>
                  <Control
                    type="text"
                    name="brand"
                    size="sm"
                    placeholder="Brand"
                    onChange={handleChange}
                    value={values.brand}
                    isInvalid={!!errors.brand}
                  />
                  {touched.brand && errors.brand ? <Feedback type="invalid">{errors.brand}</Feedback> : null}
                </Group>   
                <Group as={Col} md="4">
                  <Label>Model</Label>
                  <Control
                    type="text"
                    name="model"
                    size="sm"
                    placeholder="Model"
                    onChange={handleChange}
                    value={values.model}
                    isInvalid={!!errors.model}
                  />
                  {touched.model && errors.model ? <Feedback type="invalid">{errors.model}</Feedback> : null}
                </Group>   
              </Row>
              <Row className="mt-2"> 
                <Group as={Col} md="2">
                  <Label>Tower Form Factor</Label>
                  <Control
                    as="select"
                    name="formFactor"
                    size="sm"
                    value={values.formFactor}
                    onChange={handleChange}
                    isInvalid={!!errors.formFactor}
                  >
                    <option selected disabled>Choose...</option>
                    <option value="ATX Full Tower">ATX Full Tower</option>
                    <option value="ATX Mid Tower">ATX Mid Tower</option>
                    <option value="ATX Mini Tower">ATX Mini Tower</option>
                    <option value="MicroATX Full Tower">MicroATX Full Tower</option>
                    <option value="MicroATX Full Tower">MicroATX Full Tower</option>
                    <option value="MicroATX Full Tower">MicroATX Full Tower</option>
                    <option value="ITX Tower">ITX Tower</option>
                  </Control>
                  {touched.formFactor && errors.formFactor ? <Feedback type="invalid">{errors.formFactor}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Side Panel</Label>
                  <Control
                    as="select"
                    name="sidePanel"
                    size="sm"
                    value={values.sidePanel}
                    onChange={handleChange}
                    isInvalid={!!errors.sidePanel}
                  >
                    <option selected disabled>Choose...</option>
                    <option value="None">None</option>
                    <option value="Acrylic">Acrylic</option>
                    <option value="Tinted Acrylic">Tinted Acrylic</option>
                    <option value="Tempered Glass">Tempered Glass</option>
                    <option value="Tinted Tempered Glass">Tinted Tempered Glass</option>
                  </Control>
                  {touched.sidePanel && errors.sidePanel ? <Feedback type="invalid">{errors.sidePanel}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Color</Label>
                  <Control
                    type="text"
                    name="color"
                    size="sm"
                    placeholder="Color"
                    onChange={handleChange}
                    value={values.color}
                    isInvalid={!!errors.color}
                  />
                  {touched.color && errors.color ? <Feedback type="invalid">{errors.color}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Power Supply Shroud</Label>
                  <Control
                    as="select"
                    name="shroud"
                    size="sm"
                    value={values.shroud}
                    onChange={handleChange}
                    isInvalid={!!errors.shroud}
                  >
                    <option selected disabled>Choose...</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                  </Control>
                  {touched.shroud && errors.shroud ? <Feedback type="invalid">{errors.shroud}</Feedback> : null}
                </Group>
              </Row >
              <Row className="mt-2">
                <Group as={Col} md="3">
                  <Label>Maximum GPU Length (mm)</Label>
                  <Control
                    type="text"
                    name="maxGPULength"
                    size="sm"
                    placeholder="Maximum GPU Length (mm)"
                    onChange={handleChange}
                    value={values.maxGPULength}
                    isInvalid={!!errors.maxGPULength}
                  />
                  {touched.maxGPULength && errors.maxGPULength ? <Feedback type="invalid">{errors.maxGPULength}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Internal 2.5" Bays</Label>
                  <Control
                    type="text"
                    name="bay2"
                    size="sm"
                    placeholder='Internal 2.5" Bays'
                    onChange={handleChange}
                    value={values.bay2}
                    isInvalid={!!errors.bay2}
                  />
                  {touched.bay2 && errors.bay2 ? <Feedback type="invalid">{errors.bay2}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Internal 3.5" Bays</Label>
                  <Control
                    type="text"
                    name="bay3"
                    size="sm"
                    placeholder='Internal 3.5" Bays'
                    onChange={handleChange}
                    value={values.bay3}
                    isInvalid={!!errors.bay3}
                  />
                  {touched.bay3 && errors.bay3 ? <Feedback type="invalid">{errors.bay3}</Feedback> : null}
                </Group>
              </Row>
              <Row className="mt-2">
                <Group as={Col} md="3">
                  <Label>Supported Motherboard Form Factor</Label>
                  <Control
                    type="text"
                    name="supportedFormFactor"
                    size="sm"
                    placeholder="Eg. ATX, mATX..."
                    onChange={handleChange}
                    value={values.supportedFormFactor}
                    isInvalid={!!errors.supportedFormFactor}
                  />
                  {touched.supportedFormFactor && errors.supportedFormFactor ? <Feedback type="invalid">{errors.supportedFormFactor}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Length (mm)</Label>
                  <Control
                    type="text"
                    name="length"
                    size="sm"
                    placeholder="Length (mm)"
                    onChange={handleChange}
                    value={values.length}
                    isInvalid={!!errors.length}
                  />
                  {touched.length && errors.length ? <Feedback type="invalid">{errors.length}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Width (mm)</Label>
                  <Control
                    type="text"
                    name="width"
                    size="sm"
                    placeholder="Width (mm)"
                    onChange={handleChange}
                    value={values.width}
                    isInvalid={!!errors.width}
                  />
                  {touched.width && errors.width ? <Feedback type="invalid">{errors.width}</Feedback> : null}
                </Group>
                   <Group as={Col} md="2">
                  <Label>Height (mm)</Label>
                  <Control
                    type="text"
                    name="height"
                    size="sm"
                    placeholder="Height (mm)"
                    onChange={handleChange}
                    value={values.height}
                    isInvalid={!!errors.height}
                  />
                  {touched.height && errors.height ? <Feedback type="invalid">{errors.height}</Feedback> : null}
                </Group>
              </Row>
              <Row>
                <Group as={Col} md="2">
                  <Label>Weight (g)</Label>
                  <Control
                    type="text"
                    name="weight"
                    size="sm"
                    placeholder="Weight"
                    onChange={handleChange}
                    value={values.weight}
                    isInvalid={!!errors.weight}
                  />
                  {touched.weight && errors.weight ? <Feedback type="invalid">{errors.weight}</Feedback> : null}
                </Group>
                <Group as={Col} md="3">
                  <Label>Price (RM)</Label>
                  <Control
                    type="text"
                    name="price"
                    size="sm"
                    placeholder="Price"
                    onChange={handleChange}
                    value={values.price}
                    isInvalid={!!errors.price}
                  />
                  {touched.price && errors.price ? <Feedback type="invalid">{errors.price}</Feedback> : null}
                </Group>
              </Row>
              <Row>
                <Col md="5">
                  <Group>
                    <Label>Details 1</Label>
                    <Control
                      type="text"
                      name="detail1"
                      size="sm"
                      placeholder="Details 1"
                      onChange={handleChange}
                      value={values.detail1}
                      isInvalid={!!errors.detail1}
                    />
                    {touched.detail1 && errors.detail1 ? <Feedback type="invalid">{errors.detail1}</Feedback> : null}
                  </Group>
                  <Group>
                    <Label>Details 2</Label>
                    <Control
                      type="text"
                      name="detail2"
                      size="sm"
                      placeholder="Details 2"
                      onChange={handleChange}
                      value={values.detail2}
                      isInvalid={!!errors.detail2}
                    />
                    {touched.detail2 && errors.detail2 ? <Feedback type="invalid">{errors.detail2}</Feedback> : null}
                  </Group>
                  <Group>
                    <Label>Details 3</Label>
                    <Control
                      type="text"
                      name="detail3"
                      size="sm"
                      placeholder="Details 3"
                      onChange={handleChange}
                      value={values.detail3}
                      isInvalid={!!errors.detail3}
                    />
                    {touched.detail3 && errors.detail3 ? <Feedback type="invalid">{errors.detail3}</Feedback> : null}
                  </Group>
                </Col>
              </Row>
              <Row className="mt-4">
                <Col>
                  <Button variant="primary" type="submit">
                    Submit
                  </Button>
                </Col>
              </Row>
              </Form>           
            )}
          </Formik>
        </div>
      </div>
    );
  }
}

export default CasingEditPage;
