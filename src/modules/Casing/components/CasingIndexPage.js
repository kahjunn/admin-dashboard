import React, { Component } from 'react';
import { Table, Space, Modal, Button } from 'antd';
import { Link } from 'react-router-dom';
import moment from 'moment';
import Swal from 'sweetalert2';
import apiCaller from '../../../tools/apiUtils';
import '../../../styles/MainContainer.scss';

class CasingIndexPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      casing: []
    };
  }

  componentDidMount() {
    this.getCasing();
  }

  async getCasing() {
    try {
      const res = await apiCaller.get('api/case/');
      const casing = res.data.payload;
      this.setState({ casing });
      console.log('Casing Items', casing)
    } catch (error) {
      console.log(error);
    }
  };

  deleteCasing(casingId, casingName) {
    const id = casingId;
    const name = casingName;
    Modal.confirm({
      title: `Are you sure to remove: ${name}`,
      okText: 'Yes, delete',
      okType: 'danger',
      cancelText: 'No',
      onOk: async() => {
        await apiCaller.delete(`api/case/delete/${id}`);
        Swal.fire({
          icon: 'success',
          title: 'Delete Success',
          text: `${name} deleted sucessfully!`
        });
        const res = await apiCaller.get('api/case/');
        const casing = res.data.payload;
        this.setState({ casing });
      }
    });
  };

  render(){
    const { casing } = this.state;

    const columns = [
      {
        title: '#',
        dataIndex: 'id',
        width: '5%'
      },
      {
        title: 'Name',
        dataIndex: 'name',
        width: '30%'
      },
      {
        title: 'Brand',
        dataIndex: 'brand',
        width: '20%'
      },
      {
        title: 'Created At',
        dataIndex: 'createdAt',
        width: '13%',
        render: createdAt => (
          moment(createdAt).format('LL')
        )
      },
      {
        title: 'Price',
        dataIndex: 'price',
        render: price => (
          `RM ${price}`
        )
      },
      {
        title: 'Action',
        width: '20%',
        render: casing => (
          <span>
            <Space>
              <Link to={`/casing/edit/${casing.id}`}>
                <Button type="primary" shape="round">
                  Edit & View
                </Button>
              </Link>
              <Button type="danger" shape="round" onClick={() => this.deleteCasing(casing.id, casing.name)}>
                Delete
              </Button>
            </Space>
          </span>
        )
      }
    ]

    console.log('casing Items', casing);
    return(
      <div className="content">
        <div className="banner">
          <h1>Manage PC Parts: PC Case</h1> 
        </div>
        <div className="createButton">
          <Link to='/casing/create'>
            <button type="button" className="btn btn-success">Add a PC Case</button>
          </Link>
        </div>
        <div className="indexTable">
          <Table columns={columns} dataSource={casing} pagination={{ defaultPageSize: 12 }} />
        </div>
      </div>
    );
  }
}



export default CasingIndexPage;



