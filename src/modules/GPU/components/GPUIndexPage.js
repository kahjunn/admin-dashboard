import React, { Component } from 'react';
import { Table, Space, Modal, Button } from 'antd';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import moment from 'moment';
import apiCaller from '../../../tools/apiUtils';
import '../../../styles/MainContainer.scss';

class GPUIndexPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      GPU: []
    };
  }

  componentDidMount() {
    this.getGPU();
  }

  async getGPU() {
    try {
      const res = await apiCaller.get('api/gpu/');
      const GPU = res.data.payload;
      this.setState({ GPU });
      console.log('GPU Items', GPU)
    } catch (error) {
      console.log(error);
    }
  };

  deleteGPU(gpuId, gpuName) {
    const id = gpuId;
    const name = gpuName;
    Modal.confirm({
      title: `Are you sure to remove: ${name}`,
      okText: 'Yes, delete',
      okType: 'danger',
      cancelText: 'No',
      onOk: async() => {
        await apiCaller.delete(`api/gpu/delete/${id}`);
        Swal.fire({
          icon: 'success',
          title: 'Delete Success',
          text: `${name} deleted sucessfully!`
        });
        const res = await apiCaller.get('api/gpu/');
        const GPU = res.data.payload;
        this.setState({ GPU });
      }
    });
  };

  render(){
    const { GPU } = this.state;

    const columns = [
      {
        title: '#',
        dataIndex: 'id',
        width: '5%'
      },
      {
        title: 'Name',
        dataIndex: 'name',
        width: '30%'
      },
      {
        title: 'Brand',
        dataIndex: 'brand',
        width: '15%'
      },
      {
        title: 'Created At',
        dataIndex: 'createdAt',
        width: '18%',
        render: createdAt => (
          moment(createdAt).format('LL')
        )
      },
      {
        title: 'Price',
        dataIndex: 'price',
        width: '12%',
        render: price => (
          `RM ${price}`
        )
      },
      {
        title: 'Action',
        render: GPU => (
          <span>
            <Space>
              <Link to={`/gpu/edit/${GPU.id}`}>
                <Button type="primary" shape="round">
                  Edit & View
                </Button>
              </Link>
              <Button type="danger" shape="round" onClick={() => this.deleteGPU(GPU.id, GPU.name)}>
                Delete
              </Button>
            </Space>
          </span>
        )
      }
    ]

    console.log('CPU Items', GPU);
    return(
      <div className="content">
        <div className="banner">
          <h1>Manage PC Parts: Graphics Processing Unit (GPU)</h1> 
        </div>
        <div className="createButton">
          <Link to='/gpu/create'>
            <button type="button" className="btn btn-success">Add a GPU</button>
          </Link>
        </div>
        <div className="indexTable">
          <Table columns={columns} dataSource={GPU} pagination={{ defaultPageSize: 12 }} />
        </div>
      </div>
    );
  }
}



export default GPUIndexPage;



