import React, { Component } from 'react';
import { Button, Form, Col } from 'react-bootstrap';
import { Formik, Field, ErrorMessage } from 'formik';
import apiCaller from '../../../tools/apiUtils';
import Toast from '../../../tools/toaster';
import * as yup from 'yup';
import { Link, withRouter } from 'react-router-dom';
import '../../../styles/MainContainer.scss'
import '../styles/GPUForm.scss';

const { Group, Label, Control, Row } = Form;
const { Feedback } = Form.Control;

const gpuSchema = yup.object().shape({
  name: yup.string().required("*Field is required"),
  brand: yup.string().required("*Field is required"),
  model: yup.string().required("*Field is required"),
  chipset: yup.string().required("*Field is required"),
  memory: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  memoryType: yup.string().required("*Field is required"),
  architecture: yup.string().required("*Field is required"),
  coreClock: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  boostClock: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  gpuInterface: yup.string().required("*Selection is required"),
  sliCrossfire: yup.string().required("*Selection is required"),
  tdp: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  color: yup.string().required("*Field is required"),
  expansionSlotWidth: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  displayPorts: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  hdmiPorts: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  dviPorts: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  price: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  detail1: yup.string().required("*Field is required"),
  detail2: yup.string().required("*Field is required"),
  detail3: yup.string().required("*Field is required"),
});

class GPUCreatePage extends Component {

  handleSubmit = async(values) => {
    try {
      const formData = { ...values };
      console.log('FormData to be submitted', formData);
      await apiCaller.post('api/gpu/create', formData);
      Toast.fire({
        icon: 'success',
        title: 'GPU Added successfully '
      })
      this.props.history.push('/gpu');
    } catch (error) {
      console.log("Failed to submit");
      throw error;
    }
  };

  render() {
    return (
      <div className="gpu-content">
        <div className="banner-create">
          <h1>Add New GPU</h1>
        </div>
        <div className="backButton">
          <Link to="/gpu">
            <Button variant="warning" size="sm">Back</Button>
          </Link>
        </div>

        <div className="create-form">
        <Formik
            initialValues={{
              name: "",
              brand: "",
              model: "",
              chipset: "",
              memory: "",
              memoryType: "",
              architecture: "",
              coreClock: "",
              boostClock: "",
              gpuInterface: "",
              sliCrossfire: "",
              tdp: "",
              color: "",
              expansionSlotWidth: "",
              displayPorts: "",
              hdmiPorts: "",
              dviPorts: "",
              price: "",
              detail1: "",
              detail2: "",
              detail3: ""
            }}
            validationSchema={gpuSchema}
            onSubmit={(values, {setSubmitting, resetForm}) => {
              console.log("Form values: ", values);
              this.handleSubmit(values);
              setSubmitting(true);
              // resetForm();
              setSubmitting(false);
            }}
          >
            {({ touched, errors, values, handleChange, handleBlur, handleSubmit, isSubmitting }) => (
              <Form onSubmit={handleSubmit}>
              {console.log('Form values: ', values)}
              <Row>
                <Group as={Col} md="4">
                  <Label>Name</Label>
                  <Control
                    type="text"
                    name="name"
                    size="sm"
                    placeholder="Name"
                    onChange={handleChange}
                    value={values.name}
                    isInvalid={!!errors.name}
                  />
                  {touched.name && errors.name ? <Feedback type="invalid">{errors.name}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Brand</Label>
                  <Control
                    type="text"
                    name="brand"
                    size="sm"
                    placeholder="Brand"
                    onChange={handleChange}
                    value={values.brand}
                    isInvalid={!!errors.brand}
                  />
                  {touched.brand && errors.brand ? <Feedback type="invalid">{errors.brand}</Feedback> : null}
                </Group>   
                <Group as={Col} md="4">
                  <Label>Model</Label>
                  <Control
                    type="text"
                    name="model"
                    size="sm"
                    placeholder="Model"
                    onChange={handleChange}
                    value={values.model}
                    isInvalid={!!errors.model}
                  />
                  {touched.model && errors.model ? <Feedback type="invalid">{errors.model}</Feedback> : null}
                </Group>   
                <Group as={Col} md="2">
                  <Label>Chipset</Label>
                  <Control
                    type="text"
                    name="chipset"
                    size="sm"
                    placeholder="Eg. GTX1060"
                    onChange={handleChange}
                    value={values.chipset}
                    isInvalid={!!errors.chipset}
                  />
                  {touched.chipest && errors.chipset ? <Feedback type="invalid">{errors.chipset}</Feedback> : null}
                </Group>
              </Row>
              <Row className="mt-2"> 
                <Group as={Col} md="2">
                  <Label>Memory (GB)</Label>
                  <Control
                    type="text"
                    name="memory"
                    size="sm"
                    placeholder="Memory (GB)"
                    onChange={handleChange}
                    value={values.memory}
                    isInvalid={!!errors.memory}
                  />
                  {touched.memory && errors.memory ? <Feedback type="invalid">{errors.memory}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Memory Type</Label>
                  <Control
                    type="text"
                    name="memoryType"
                    size="sm"
                    placeholder="Eg: GDDR6"
                    onChange={handleChange}
                    value={values.memoryType}
                    isInvalid={!!errors.memoryType}
                  />
                  {touched.memoryType && errors.memoryType ? <Feedback type="invalid">{errors.memoryType}</Feedback> : null}
                </Group>
                <Group as={Col} md="4">
                  <Label>Architecture</Label>
                  <Control
                    type="text"
                    name="architecture"
                    size="sm"
                    placeholder="Eg. Pascal"
                    onChange={handleChange}
                    value={values.architecture}
                    isInvalid={!!errors.architecture}
                  />
                  {touched.architecture && errors.architecture ? <Feedback type="invalid">{errors.architecture}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Core Clock (MHz)</Label>
                  <Control
                    type="text"
                    name="coreClock"
                    size="sm"
                    placeholder="Core Clock"
                    onChange={handleChange}
                    value={values.coreClock}
                    isInvalid={!!errors.coreClock}
                  />
                  {touched.coreClock && errors.coreClock ? <Feedback type="invalid">{errors.coreClock}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Boost Clock (MHz)</Label>
                  <Control
                    type="text"
                    name="boostClock"
                    size="sm"
                    placeholder="Boost Clock"
                    onChange={handleChange}
                    value={values.boostClock}
                    isInvalid={!!errors.boostClock}
                  />
                  {touched.boostClock && errors.boostClock ? <Feedback type="invalid">{errors.boostClock}</Feedback> : null}
                </Group>
              </Row >
              <Row className="mt-2">
                <Group as={Col} md="2">
                  <Label>Interface</Label>
                  <Control
                    as="select"
                    name="gpuInterface"
                    size="sm"
                    onChange={handleChange}
                    isInvalid={!!errors.gpuInterface}
                  >
                    <option selected disabled>Choose...</option>
                    <option value="PCIe x16">PCIe x16</option>
                    <option value="PCIe x8">PCIe x8</option>
                    <option value="PCIe x1">PCIe x1</option>
                    <option value="AGP">AGP</option>
                    <option value="PCI">PCI</option>
                  </Control>
                  {touched.gpuInterface && errors.gpuInterface ? <Feedback type="invalid">{errors.gpuInterface}</Feedback> : null}
                </Group>
                <Group as={Col} md="3">
                  <Label>SLI/Crossfire Capability</Label>
                  <Control
                    as="select"
                    name="sliCrossfire"
                    size="sm"
                    placeholder="SLI/Crossfire"
                    onChange={handleChange}
                    isInvalid={!!errors.sliCrossfire}
                  >
                    <option selected disabled>Choose...</option>
                    <option value="SLI Capable">SLI Capable</option>
                    <option value="Crossfire Capable">Crossfire Capable</option>
                  </Control>
                  {touched.sliCrossfire && errors.sliCrossfire ? <Feedback type="invalid">{errors.sliCrossfire}</Feedback> : null}
                </Group>
                <Group as={Col} md="3">
                  <Label>TDP (Watt)</Label>
                  <Control
                    type="text"
                    name="tdp"
                    size="sm"
                    placeholder="TDP"
                    onChange={handleChange}
                    value={values.tdp}
                    isInvalid={!!errors.tdp}
                  />
                  {touched.tdp && errors.tdp ? <Feedback type="invalid">{errors.tdp}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Color</Label>
                  <Control
                    type="text"
                    name="color"
                    size="sm"
                    placeholder="Color"
                    onChange={handleChange}
                    value={values.color}
                    isInvalid={!!errors.color}
                  />
                  {touched.color && errors.color ? <Feedback type="invalid">{errors.color}</Feedback> : null}
                </Group>
              </Row>
              <Row className="mt-2">
                <Group as={Col} md="3">
                  <Label>Expansion Slot Width (No.)</Label>
                  <Control
                    type="text"
                    name="expansionSlotWidth"
                    size="sm"
                    placeholder="Expansion Slot Width"
                    onChange={handleChange}
                    value={values.expansionSlotWidth}
                    isInvalid={!!errors.expansionSlotWidth}
                  />
                  {touched.expansionSlotWidth && errors.expansionSlotWidth ? <Feedback type="invalid">{errors.expansionSlotWidth}</Feedback> : null}
                </Group>
                <Group as={Col} md="3">
                  <Label>Display Ports (No.)</Label>
                  <Control
                    type="text"
                    name="displayPorts"
                    size="sm"
                    placeholder="Display Ports"
                    onChange={handleChange}
                    value={values.displayPorts}
                    isInvalid={!!errors.displayPorts}
                  />
                  {touched.displayPorts && errors.displayPorts ? <Feedback type="invalid">{errors.displayPorts}</Feedback> : null}
                </Group>
                <Group as={Col} md="3">
                  <Label>DVI Ports (No.)</Label>
                  <Control
                    type="text"
                    name="dviPorts"
                    size="sm"
                    placeholder="DVI Ports"
                    onChange={handleChange}
                    value={values.dviPorts}
                    isInvalid={!!errors.dviPorts}
                  />
                  {touched.dviPorts && errors.dviPorts ? <Feedback type="invalid">{errors.dviPorts}</Feedback> : null}
                </Group>
                <Group as={Col} md="3">
                  <Label>HDMI Ports (No.)</Label>
                  <Control
                    type="text"
                    name="hdmiPorts"
                    size="sm"
                    placeholder="HDMI Ports"
                    onChange={handleChange}
                    value={values.hdmiPorts}
                    isInvalid={!!errors.hdmiPorts}
                  />
                  {touched.hdmiPorts && errors.hdmiPorts ? <Feedback type="invalid">{errors.hdmiPorts}</Feedback> : null}
                </Group>
              </Row>
              <Row>
                <Group as={Col} md="3">
                  <Label>Price (RM)</Label>
                  <Control
                    type="text"
                    name="price"
                    size="sm"
                    placeholder="Price"
                    onChange={handleChange}
                    value={values.price}
                    isInvalid={!!errors.price}
                  />
                  {touched.price && errors.price ? <Feedback type="invalid">{errors.price}</Feedback> : null}
                </Group>
              </Row>
              <Row>
                <Col md="5">
                  <Group>
                    <Label>Details 1</Label>
                    <Control
                      type="text"
                      name="detail1"
                      size="sm"
                      placeholder="Details 1"
                      onChange={handleChange}
                      value={values.detail1}
                      isInvalid={!!errors.detail1}
                    />
                    {touched.detail1 && errors.detail1 ? <Feedback type="invalid">{errors.detail1}</Feedback> : null}
                  </Group>
                  <Group>
                    <Label>Details 2</Label>
                    <Control
                      type="text"
                      name="detail2"
                      size="sm"
                      placeholder="Details 2"
                      onChange={handleChange}
                      value={values.detail2}
                      isInvalid={!!errors.detail2}
                    />
                    {touched.detail2 && errors.detail2 ? <Feedback type="invalid">{errors.detail2}</Feedback> : null}
                  </Group>
                  <Group>
                    <Label>Details 3</Label>
                    <Control
                      type="text"
                      name="detail3"
                      size="sm"
                      placeholder="Details 3"
                      onChange={handleChange}
                      value={values.detail3}
                      isInvalid={!!errors.detail3}
                    />
                    {touched.detail3 && errors.detail3 ? <Feedback type="invalid">{errors.detail3}</Feedback> : null}
                  </Group>
                </Col>
              </Row>
              <Row className="mt-4">
                <Col>
                  <Button variant="primary" type="submit">
                    Submit
                  </Button>
                </Col>
              </Row>
              </Form>           
            )}
          </Formik>
        </div>


      </div>
    );
  }
}

export default GPUCreatePage;
