import React, { Component } from 'react';
import { Button, Form, Col } from 'react-bootstrap';
import { Formik, Field, ErrorMessage } from 'formik';
import apiCaller from '../../../tools/apiUtils';
import Toast from '../../../tools/toaster';
import * as yup from 'yup';
import { Link, withRouter } from 'react-router-dom';
import Thumb from '../../../tools/Thumb';
import '../../../styles/MainContainer.scss'
import '../styles/BuildsForm.scss';

const { Group, Label, Control, Row } = Form;
const { Feedback } = Form.Control;

const buildSchema = yup.object().shape({
  name: yup.string().required("*Field is required"),
  cpu: yup.string().required("*Field is required"),
  mobo: yup.string().required("*Field is required"),
  memory: yup.string().required("*Field is required"),
  storage: yup.string().required("*Field is required"),
  gpu: yup.string().required("*Field is required"),
  casing: yup.string().required("*Field is required"),
  psu: yup.string().required("*Field is required"),
  os: yup.string().required("*Field is required"),
  cooler: yup.string().required("*Field is required"),
  price: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  description: yup.string().required("*Field is required"),
});

class BuildsEditPage extends Component {
  constructor(props) {
    super(props); 
    this.state = {
      buildId: props.match.params.id,
      build: {}
    };
  }

  componentDidMount() {
    this.getCPU();
  }

  async getCPU() {
    const res = await apiCaller.get(`/api/pcbuild/${this.state.buildId}`);
    const build = res.data.payload;
    this.setState({ build });
  }

  handleSubmit = async(values) => {
    try {
      const formData = { ...values };
      console.log('FormData to be submitted', formData);
      await apiCaller.put(`api/pcbuild/update/${this.state.buildId}`, formData);
      const res = await apiCaller.get(`/api/pcbuild/${this.state.buildId}`);
      const build = res.data.payload;
      this.setState({ build });
      Toast.fire({
        icon: 'success',
        title: `Update Success: ${this.state.build.name}`
      })
      this.props.history.push('/pcbuilds');
    } catch (error) {
      console.log("Failed to submit");
      throw error;
    }
  };

  render() {
    const { build } = this.state;
    return (
      <div className="build-content">
        <div className="banner-create">
          <h1>Edit PC Build: {build.name}</h1>
        </div>
        <div className="backButton">
          <Link to="/pcbuilds">
            <Button variant="warning" size="sm">Back</Button>
          </Link>
        </div>

        <div className="create-form">
          <Formik
            initialValues={build}
            enableReinitialize
            validationSchema={buildSchema}
            onSubmit={(values, {setSubmitting, resetForm}) => {
              console.log("Form values: ", values);
              this.handleSubmit(values);
              setSubmitting(true);
              // resetForm();
              setSubmitting(false);
            }}
          >
            {({ touched, errors, values, handleChange, handleBlur, handleSubmit, isSubmitting, setFieldValue }) => (
              <Form onSubmit={handleSubmit}>
              {console.log(values)}
              <Row>
                <Col md="6">
                  <Group>
                    <Label>Name</Label>
                    <Control
                      type="text"
                      name="name"
                      size="sm"
                      placeholder="Name"
                      onChange={handleChange}
                      value={values.name}
                      isInvalid={!!errors.name}
                    />
                    {touched.name && errors.name ? <Feedback type="invalid">{errors.name}</Feedback> : null}
                  </Group>
                  <Group>
                    <Label>Computer Processing Unit (CPU)</Label>
                    <Control
                      type="text"
                      name="cpu"
                      size="sm"
                      placeholder="Eg. Intel i7-8400K"
                      onChange={handleChange}
                      value={values.cpu}
                      isInvalid={!!errors.cpu}
                    />
                    {touched.cpu && errors.cpu ? <Feedback type="invalid">{errors.cpu}</Feedback> : null}
                  </Group>   
                  <Group>
                    <Label>Motherboard</Label>
                    <Control
                      type="text"
                      name="mobo"
                      size="sm"
                      placeholder="Eg. Gigabyte Aorus B450"
                      onChange={handleChange}
                      value={values.mobo}
                      isInvalid={!!errors.mobo}
                    />
                    {touched.mobo && errors.mobo ? <Feedback type="invalid">{errors.mobo}</Feedback> : null}
                  </Group> 
                  <Group>
                    <Label>Memory (RAM)</Label>
                    <Control
                      type="text"
                      name="memory"
                      size="sm"
                      placeholder="Eg. G.Skill Trident Z DDR4 3200 MHz (2 x 8 GB)"
                      onChange={handleChange}
                      value={values.memory}
                      isInvalid={!!errors.memory}
                    />
                    {touched.memory && errors.memory ? <Feedback type="invalid">{errors.memory}</Feedback> : null}
                  </Group>
                  <Group>
                    <Label>Storage</Label>
                    <Control
                      type="text"
                      name="storage"
                      size="sm"
                      placeholder="Eg. Samsung Evo 970 Plus 500GB"
                      onChange={handleChange}
                      value={values.storage}
                      isInvalid={!!errors.storage}
                    />
                    {touched.storage && errors.storage ? <Feedback type="invalid">{errors.storage}</Feedback> : null}
                  </Group>
                  <Group>
                    <Label>Graphics Processing Unit (GPU)</Label>
                    <Control
                      type="text"
                      name="gpu"
                      size="sm"
                      placeholder="Eg. Asus ROG Strix RTX2060 6GB"
                      onChange={handleChange}
                      value={values.gpu}
                      isInvalid={!!errors.gpu}
                    />
                    {touched.gpu && errors.gpu ? <Feedback type="invalid">{errors.gpu}</Feedback> : null}
                  </Group>
                  <Group>
                    <Label>PC Case</Label>
                    <Control
                      type="text"
                      name="casing"
                      size="sm"
                      placeholder="Eg. NZXT H500i (White)"
                      onChange={handleChange}
                      value={values.casing}
                      isInvalid={!!errors.casing}
                    />
                    {touched.casing && errors.casing ? <Feedback type="invalid">{errors.casing}</Feedback> : null}
                  </Group>
                  <Group>
                    <Label>Power Supply Unit (PSU)</Label>
                    <Control
                      type="text"
                      name="psu"
                      size="sm"
                      placeholder="Eg. Cooler Master MWE Gold 750W "
                      onChange={handleChange}
                      value={values.psu}
                      isInvalid={!!errors.psu}
                    />
                    {touched.psu && errors.psu ? <Feedback type="invalid">{errors.psu}</Feedback> : null}
                  </Group>
                  <Group>
                    <Label>Operating System (OS)</Label>
                    <Control
                      type="text"
                      name="os"
                      size="sm"
                      placeholder="Eg. Microsoft Windows 10 64 bit"
                      onChange={handleChange}
                      value={values.os}
                      isInvalid={!!errors.os}
                    />
                    {touched.os && errors.os ? <Feedback type="invalid">{errors.os}</Feedback> : null}
                  </Group>
                  <Group>
                    <Label>CPU Cooler</Label>
                    <Control
                      type="text"
                      name="cooler"
                      size="sm"
                      placeholder="Eg. NZXT Kraken X52 Rev 2 AIO Cooler"
                      onChange={handleChange}
                      value={values.cooler}
                      isInvalid={!!errors.cooler}
                    />
                    {touched.cooler && errors.cooler ? <Feedback type="invalid">{errors.cooler}</Feedback> : null}
                  </Group>
                  <Group>
                    <Label>Price (RM)</Label>
                    <Control
                      type="text"
                      name="price"
                      size="sm"
                      placeholder="Price"
                      onChange={handleChange}
                      value={values.price}
                      isInvalid={!!errors.price}
                    />
                    {touched.price && errors.price ? <Feedback type="invalid">{errors.price}</Feedback> : null}
                  </Group>
                </Col>  
              </Row>
              <Row>
                <Col md="10">
                  <Group>
                    <Label>Build Description</Label>
                    <Control
                      type="text"
                      name="description"
                      size="sm"
                      placeholder="Build Description"
                      onChange={handleChange}
                      value={values.description}
                      isInvalid={!!errors.description}
                    />
                    {touched.description && errors.description ? <Feedback type="invalid">{errors.description}</Feedback> : null}
                  </Group>
                </Col>
              </Row>
              <Row>  
                <Group as={Col} md="4">
                  <Label>Upload Image</Label>
                  <Control
                    type="file"
                    name="file"
                    size="sm"
                    onChange={(e) => {
                      setFieldValue("file", e.currentTarget.files[0]);
                    }}
                  />
                <Thumb file={values.file} />
                </Group> 
              </Row>
              <Row className="mt-4">
                <Col>
                  <Button variant="primary" type="submit">
                    Submit
                  </Button>
                </Col>
              </Row>
              </Form>           
            )}
          </Formik>
        </div>
      </div>
    );
  }
}

export default BuildsEditPage;
