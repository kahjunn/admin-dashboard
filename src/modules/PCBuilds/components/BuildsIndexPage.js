import React, { Component } from 'react';
import { Table, Space, Modal, Button } from 'antd';
import { Link } from 'react-router-dom';
import moment from 'moment';
import Swal from 'sweetalert2';
import apiCaller from '../../../tools/apiUtils';
import '../../../styles/MainContainer.scss';

class BuildsIndexPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      build: []
    };
  }

  componentDidMount() {
    this.getBuild();
  }

  async getBuild() {
    try {
      const res = await apiCaller.get('api/pcbuild/');
      const build = res.data.payload;
      this.setState({ build });
      console.log('build Items', build)
    } catch (error) {
      console.log(error);
    }
  };

  deleteBuild(buildId, buildName) {
    const id = buildId;
    const name = buildName;
    Modal.confirm({
      title: `Are you sure to remove: ${name}`,
      okText: 'Yes, delete',
      okType: 'danger',
      cancelText: 'No',
      onOk: async() => {
        await apiCaller.delete(`api/pcbuild/delete/${id}`);
        const res = await apiCaller.get('api/pcbuild/');
        const build = res.data.payload;
        this.setState({ build });
      }
    });
  };

  render(){
    const { build } = this.state;

    const columns = [
      {
        title: '#',
        dataIndex: 'id',
        width: '10%'
      },
      {
        title: 'Name',
        dataIndex: 'name',
        width: '25%'
      },
      {
        title: 'Price',
        dataIndex: 'price',
        width: '20%',
        render: price => (
          `RM ${price}`
        )
      },
      {
        title: 'Date Created',
        dataIndex: 'createdAt',
        width: '20%',
        render: createdAt => (
          moment(createdAt).format('LL')
        )
      },
      {
        title: 'Action',
        width: '20%',
        render: build => (
          <span>
            <Space>
              <Link to={`/pcbuilds/edit/${build.id}`}>
                <Button type="primary" shape="round">
                  Edit & View
                </Button>
              </Link>
              <Button type="danger" shape="round" onClick={() => this.deleteBuild(build.id, build.name)}>
                Delete
              </Button>
            </Space>
          </span>
        )
      }
    ]

    console.log('build Items', build);
    return(
      <div className="content">
        <div className="banner">
          <h1>Manage PC Builds</h1> 
        </div>
        <div className="createButton">
          <Link to='/pcbuilds/create'>
            <button type="button" className="btn btn-success">Add a PC Build</button>
          </Link>
        </div>
        <div className="indexTable">
          <Table columns={columns} dataSource={build} pagination={{ defaultPageSize: 12 }} />
        </div>
      </div>
    );
  }
}



export default BuildsIndexPage;



