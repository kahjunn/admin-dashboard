import React, { Component } from 'react';
import { Button, Form, Col } from 'react-bootstrap';
import { Formik, Field, ErrorMessage } from 'formik';
import apiCaller from '../../../tools/apiUtils';
import Toast from '../../../tools/toaster';
import * as yup from 'yup';
import { Link, withRouter } from 'react-router-dom';
import '../../../styles/MainContainer.scss'
import '../styles/MOBOForm.scss';

const { Group, Label, Control, Row } = Form;
const { Feedback } = Form.Control;

const moboSchema = yup.object().shape({
  name: yup.string().required("*Field is required"),
  brand: yup.string().required("*Field is required"),
  model: yup.string().required("*Field is required"),
  formFactor: yup.string().required("*Selection is required"),
  socket: yup.string().required("*Field is required"),
  chipset: yup.string().required("*Field is required"),
  maxMemory: yup.string().required("*Field is required"),
  maxMemorySlot: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  color: yup.string().required("*Field is required"),
  sliCrossfire: yup.string().required("*Field is required"),
  pcie16Slot: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  pcie8Slot: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  m2Slot: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  sataPort: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  onBoardEthernet: yup.string().required("*Selection is required"),
  price: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  detail1: yup.string().required("*Field is required"),
  detail2: yup.string().required("*Field is required"),
  detail3: yup.string().required("*Field is required"),
});

class MOBOEditPage extends Component {
  constructor(props) {
    super(props); 
    this.state = {
      moboId: props.match.params.id,
      mobo: {}
    };
  }

  componentDidMount() {
    this.getMobo();
  }

  async getMobo() {
    const res = await apiCaller.get(`/api/mobo/${this.state.moboId}`);
    const mobo = res.data.payload;
    this.setState({ mobo });
  }
  
  handleSubmit = async(values) => {
    try {
      const formData = { ...values };
      console.log('FormData to be submitted', formData);
      await apiCaller.put(`api/mobo/update/${this.state.moboId}`, formData);
      const res = await apiCaller.get(`/api/mobo/${this.state.moboId}`);
      const mobo = res.data.payload;
      this.setState({ mobo });
      Toast.fire({
        icon: 'success',
        title: `Update Success: ${this.state.mobo.name}`
      })
      this.props.history.push('/mobo');
    } catch (error) {
      console.log("Failed to submit");
      throw error;
    }
  };

  render() {
    const { mobo } = this.state;
    console.log('Values: ', mobo);
    return (
      <div className="mobo-content">
        <div className="banner-create">
          <h1>Edit Motherboard: {mobo.name}</h1>
        </div>
        <div className="backButton">
          <Link to="/mobo">
            <Button variant="warning" size="sm">Back</Button>
          </Link>
        </div>

        <div className="create-form">
        <Formik
            initialValues={mobo}
            enableReinitialize
            validationSchema={moboSchema}
            onSubmit={(values, {setSubmitting, resetForm}) => {
              console.log("Form values: ", values);
              this.handleSubmit(values);
              setSubmitting(true);
              // resetForm();
              setSubmitting(false);
            }}
          >
            {({ touched, errors, values, handleChange, handleBlur, handleSubmit, isSubmitting }) => (
              <Form onSubmit={handleSubmit}>
              {console.log(values)}
              <Row>
                <Group as={Col} md="4">
                  <Label>Name</Label>
                  <Control
                    type="text"
                    name="name"
                    size="sm"
                    placeholder="Name"
                    onChange={handleChange}
                    value={values.name}
                    isInvalid={!!errors.name}
                  />
                  {touched.name && errors.name ? <Feedback type="invalid">{errors.name}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Brand</Label>
                  <Control
                    type="text"
                    name="brand"
                    size="sm"
                    placeholder="Brand"
                    onChange={handleChange}
                    value={values.brand}
                    isInvalid={!!errors.brand}
                  />
                  {touched.brand && errors.brand ? <Feedback type="invalid">{errors.brand}</Feedback> : null}
                </Group>   
                <Group as={Col} md="4">
                  <Label>Model</Label>
                  <Control
                    type="text"
                    name="model"
                    size="sm"
                    placeholder="Model"
                    onChange={handleChange}
                    value={values.model}
                    isInvalid={!!errors.model}
                  />
                  {touched.model && errors.model ? <Feedback type="invalid">{errors.model}</Feedback> : null}
                </Group>   
                <Group as={Col} md="2">
                  <Label>Form Factor</Label>
                  <Control
                    as="select"
                    name="formFactor"
                    size="sm"
                    value={values.formFactor}
                    onChange={handleChange}
                    isInvalid={!!errors.formFactor}
                  >
                    <option disabled>Choose...</option>
                    <option value="ATX">ATX</option>
                    <option value="Micro ATX">Micro ATX</option>
                    <option value="ITX">ITX</option>
                  </Control>
                  {touched.formFactor && errors.formFactor ? <Feedback type="invalid">{errors.formFactor}</Feedback> : null}
                </Group>
              </Row>
              <Row className="mt-2"> 
                <Group as={Col} md="2">
                  <Label>Socket</Label>
                  <Control
                    type="text"
                    name="socket"
                    size="sm"
                    placeholder="Eg. LGA 1151"
                    onChange={handleChange}
                    value={values.socket}
                    isInvalid={!!errors.socket}
                  />
                  {touched.socket && errors.socket ? <Feedback type="invalid">{errors.socket}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Chipset</Label>
                  <Control
                    type="text"
                    name="chipset"
                    size="sm"
                    placeholder="Eg. Intel B430"
                    onChange={handleChange}
                    value={values.chipset}
                    isInvalid={!!errors.chipset}
                  />
                  {touched.chipset && errors.chipset ? <Feedback type="invalid">{errors.chipset}</Feedback> : null}
                </Group>
                <Group as={Col} md="4">
                  <Label>Max Supported Memory (GB)</Label>
                  <Control
                    type="text"
                    name="maxMemory"
                    size="sm"
                    placeholder="Max Supported Memory"
                    onChange={handleChange}
                    value={values.maxMemory}
                    isInvalid={!!errors.maxMemory}
                  />
                  {touched.maxMemory && errors.maxMemory ? <Feedback type="invalid">{errors.maxMemory}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Max Memory Slot (No.)</Label>
                  <Control
                    type="text"
                    name="maxMemorySlot"
                    size="sm"
                    placeholder="Max Memory Slot"
                    onChange={handleChange}
                    value={values.maxMemorySlot}
                    isInvalid={!!errors.maxMemorySlot}
                  />
                  {touched.maxMemorySlot && errors.maxMemorySlot ? <Feedback type="invalid">{errors.maxMemorySlot}</Feedback> : null}
                </Group>
              </Row >
              <Row className="mt-2">
                <Group as={Col} md="4">
                  <Label>Color</Label>
                  <Control
                    type="text"
                    name="color"
                    size="sm"
                    placeholder="Color"
                    onChange={handleChange}
                    value={values.color}
                    isInvalid={!!errors.color}
                  />
                  {touched.color && errors.color ? <Feedback type="invalid">{errors.color}</Feedback> : null}
                </Group>
                <Group as={Col} md="4">
                  <Label>SLI/Crossfire Capability</Label>
                  <Control
                    as="select"
                    name="sliCrossfire"
                    size="sm"
                    value={values.sliCrossfire}
                    onChange={handleChange}
                    isInvalid={!!errors.sliCrossfire}
                  >
                    <option disabled>Choose...</option>
                    <option value="SLI Capable">Yes</option>
                    <option value="Crossfire Capable">No</option>
                  </Control>
                  {touched.sliCrossfire && errors.sliCrossfire ? <Feedback type="invalid">{errors.sliCrossfire}</Feedback> : null}
                </Group>
                <Group as={Col} md="3">
                  <Label>Onboard Ethernet</Label>
                  <Control
                    as="select"
                    name="onBoardEthernet"
                    size="sm"
                    value={values.onBoardEthernet}
                    onChange={handleChange}
                    isInvalid={!!errors.onBoardEthernet}
                  >
                    <option disabled>Choose...</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                  </Control>
                  {touched.onBoardEthernet && errors.onBoardEthernet ? <Feedback type="invalid">{errors.onBoardEthernet}</Feedback> : null}
                </Group>
              </Row>
              <Row className="mt-2">
                <Group as={Col} md="3">
                  <Label>PCIe x16 Slots (No.)</Label>
                  <Control
                    type="text"
                    name="pcieSlot"
                    size="sm"
                    placeholder="PCIe x16 Slots"
                    onChange={handleChange}
                    value={values.pcieSlot}
                    isInvalid={!!errors.pcieSlot}
                  />
                  {touched.pcieSlot && errors.pcieSlot ? <Feedback type="invalid">{errors.pcieSlot}</Feedback> : null}
                </Group>
                <Group as={Col} md="3">
                  <Label>PCIe x8 Slots (No.)</Label>
                  <Control
                    type="text"
                    name="pcie8Slot"
                    size="sm"
                    placeholder="PCIe x8 Slots"
                    onChange={handleChange}
                    value={values.pcie8Slot}
                    isInvalid={!!errors.pcie8Slot}
                  />
                  {touched.pcie8Slot && errors.pcie8Slot ? <Feedback type="invalid">{errors.pcie8Slot}</Feedback> : null}
                </Group>
                <Group as={Col} md="3">
                  <Label>M.2 Slots(No.)</Label>
                  <Control
                    type="text"
                    name="m2Slot"
                    size="sm"
                    placeholder="M.2 Slots"
                    onChange={handleChange}
                    value={values.m2Slot}
                    isInvalid={!!errors.m2Slot}
                  />
                  {touched.m2Slot && errors.m2Slot ? <Feedback type="invalid">{errors.m2Slot}</Feedback> : null}
                </Group>
                <Group as={Col} md="3">
                  <Label>SATA 6 GB/s Ports (No.)</Label>
                  <Control
                    type="text"
                    name="sataPort"
                    size="sm"
                    placeholder="SATA 6 GB/s Ports"
                    onChange={handleChange}
                    value={values.sataPort}
                    isInvalid={!!errors.sataPort}
                  />
                  {touched.sataPort && errors.sataPort ? <Feedback type="invalid">{errors.sataPort}</Feedback> : null}
                </Group>
              </Row>
              <Row>
                <Group as={Col} md="3">
                  <Label>Price (RM)</Label>
                  <Control
                    type="text"
                    name="price"
                    size="sm"
                    placeholder="Price"
                    onChange={handleChange}
                    value={values.price}
                    isInvalid={!!errors.price}
                  />
                  {touched.price && errors.price ? <Feedback type="invalid">{errors.price}</Feedback> : null}
                </Group>
              </Row> 
              <Row>
                <Col md="5">
                  <Group>
                    <Label>Details 1</Label>
                    <Control
                      type="text"
                      name="detail1"
                      size="sm"
                      placeholder="Details 1"
                      onChange={handleChange}
                      value={values.detail1}
                      isInvalid={!!errors.detail1}
                    />
                    {touched.detail1 && errors.detail1 ? <Feedback type="invalid">{errors.detail1}</Feedback> : null}
                  </Group>
                  <Group>
                    <Label>Details 2</Label>
                    <Control
                      type="text"
                      name="detail2"
                      size="sm"
                      placeholder="Details 2"
                      onChange={handleChange}
                      value={values.detail2}
                      isInvalid={!!errors.detail2}
                    />
                    {touched.detail2 && errors.detail2 ? <Feedback type="invalid">{errors.detail2}</Feedback> : null}
                  </Group>
                  <Group>
                    <Label>Details 3</Label>
                    <Control
                      type="text"
                      name="detail3"
                      size="sm"
                      placeholder="Details 3"
                      onChange={handleChange}
                      value={values.detail3}
                      isInvalid={!!errors.detail3}
                    />
                    {touched.detail3 && errors.detail3 ? <Feedback type="invalid">{errors.detail3}</Feedback> : null}
                  </Group>
                </Col>
              </Row>
              <Row className="mt-4">
                <Col>
                  <Button variant="primary" type="submit">
                    Submit & Update
                  </Button>
                </Col>
              </Row>
              </Form>           
            )}
          </Formik>
        </div>
      </div>
    );
  }
}

export default MOBOEditPage;
