import React, { Component } from 'react';
import { Table, Space, Modal, Button } from 'antd';
import { Link } from 'react-router-dom';
import moment from 'moment';
import Swal from 'sweetalert2';
import apiCaller from '../../../tools/apiUtils';
import '../../../styles/MainContainer.scss';

class MOBOIndexPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      MOBO: []
    };
  }

  componentDidMount() {
    this.getMOBO();
  }

  async getMOBO() {
    try {
      const res = await apiCaller.get('api/mobo/');
      const MOBO = res.data.payload;
      this.setState({ MOBO });
      console.log('Mobo Items', MOBO)
    } catch (error) {
      console.log(error);
    }
  };

  deleteMOBO(moboId, moboName) {
    const id = moboId;
    const name = moboName;
    Modal.confirm({
      title: `Are you sure to remove: ${name}`,
      okText: 'Yes, delete',
      okType: 'danger',
      cancelText: 'No',
      onOk: async() => {
        await apiCaller.delete(`api/mobo/delete/${id}`);
        Swal.fire({
          icon: 'success',
          title: 'Delete Success',
          text: `${name} deleted sucessfully!`
        });
        const res = await apiCaller.get('api/mobo/');
        const MOBO = res.data.payload;
        this.setState({ MOBO });
      }
    });
  };

  render(){
    const { MOBO } = this.state;

    const columns = [
      {
        title: '#',
        dataIndex: 'id',
        width: '5%'
      },
      {
        title: 'Name',
        dataIndex: 'name',
        width: '30%'
      },
      {
        title: 'Brand',
        dataIndex: 'brand',
        width: '11%'
      },
      {
        title: 'Created At',
        dataIndex: 'createdAt',
        width: '15%',
        render: createdAt => (
          moment(createdAt).format('LL')
        )
      },
      {
        title: 'Price',
        dataIndex: 'price',
        width: '13%',
        render: price => (
          `RM ${price}`
        )
      },
      {
        title: 'Action',
        width: '20%',
        render: MOBO => (
          <span>
            <Space>
              <Link to={`/mobo/edit/${MOBO.id}`}>
                <Button type="primary" shape="round">
                  Edit & View
                </Button>
              </Link>
              <Button type="danger" shape="round" onClick={() => this.deleteMOBO(MOBO.id, MOBO.name)}>
                Delete
              </Button>
            </Space>
          </span>
        )
      }
    ]

    console.log('Motherboard Items', MOBO);
    return(
      <div className="content">
        <div className="banner">
          <h1>Manage PC Parts: Motherboard</h1> 
        </div>
        <div className="createButton">
          <Link to='/mobo/create'>
            <button type="button" className="btn btn-success">Add a Motherboard</button>
          </Link>
        </div>
        <div className="indexTable">
          <Table columns={columns} dataSource={MOBO} pagination={{ defaultPageSize: 12 }} />
        </div>
      </div>
    );
  }
}



export default MOBOIndexPage;



