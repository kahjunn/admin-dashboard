import React, { Component } from 'react';
import { Table, Space, Modal, Button } from 'antd';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import moment from 'moment';
import apiCaller from '../../../tools/apiUtils';
import '../../../styles/MainContainer.scss';

class CPUIndexPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      CPU: []
    };
  }

  componentDidMount() {
    this.getCPU();
  }

  async getCPU() {
    try {
      const res = await apiCaller.get('api/cpu/');
      const CPU = res.data.payload;
      this.setState({ CPU });
      console.log('CPU Items', CPU)
    } catch (error) {
      console.log(error);
    }
  };

  deleteCPU(cpuId, cpuName) {
    const id = cpuId;
    const name = cpuName;
    Modal.confirm({
      title: `Are you sure to remove: ${name}`,
      okText: 'Yes, delete',
      okType: 'danger',
      cancelText: 'No',
      onOk: async() => {
        await apiCaller.delete(`api/cpu/delete/${id}`);
        Swal.fire({
          icon: 'success',
          title: 'Delete Success',
          text: `${name} deleted sucessfully!`
        });
        const res = await apiCaller.get('api/cpu/');
        const CPU = res.data.payload;
        this.setState({ CPU });
      }
    });
  };

  render(){
    const { CPU } = this.state;

    const columns = [
      {
        title: '#',
        dataIndex: 'id',
        width: '5%'
      },
      {
        title: 'Name',
        dataIndex: 'name',
        width: '25%'
      },
      {
        title: 'Brand',
        dataIndex: 'brand',
        width: '15%'
      },
      {
        title: 'Created At',
        dataIndex: 'createdAt',
        width: '20%',
        render: createdAt => (
          moment(createdAt).format('LL')
        )
      },
      {
        title: 'Price',
        dataIndex: 'price',
        width: '15%',
        render: price => (
          `RM ${price}`
        )
      },
      {
        title: 'Action',
        width: '20%',
        render: CPU => (
          <span>
            <Space>
              <Link to={`/cpu/edit/${CPU.id}`}>
                <Button type="primary" shape="round">
                  Edit & View
                </Button>
              </Link>
              <Button type="danger" shape="round" onClick={() => this.deleteCPU(CPU.id, CPU.name)}>
                Delete
              </Button>
            </Space>
          </span>
        )
      }
    ]

    console.log('CPU Items', CPU);
    return(
      <div className="content">
        <div className="banner">
          <h1>Manage PC Parts: Computer Processing Unit (CPU)</h1> 
        </div>
        <div className="createButton">
          <Link to='/cpu/create'>
            <button type="button" className="btn btn-success">Add a CPU</button>
          </Link>
        </div>
        <div className="indexTable">
          <Table columns={columns} dataSource={CPU} pagination={{ defaultPageSize: 12 }} />
        </div>
      </div>
    );
  }
}



export default CPUIndexPage;



