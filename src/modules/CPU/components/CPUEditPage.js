import React, { Component } from 'react';
import { Button, Form, Col } from 'react-bootstrap';
import { Formik, Field, ErrorMessage } from 'formik';
import apiCaller from '../../../tools/apiUtils';
import Toast from '../../../tools/toaster';
import * as yup from 'yup';
import { Link, withRouter } from 'react-router-dom';
import Thumb from '../../../tools/Thumb';
import '../../../styles/MainContainer.scss'
import '../styles/CPUForm.scss';

const { Group, Label, Control, Row } = Form;
const { Feedback } = Form.Control;

const cpuSchema = yup.object().shape({
  name: yup.string().required("*Field is required"),
  brand: yup.string().required("*Field is required"),
  model: yup.string().required("*Field is required"),
  coreCount: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  clockSpeed: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  boostClock: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  supportedSocket: yup.string().required("*Field is required"),
  threads: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  coreFamily: yup.string().required("*Field is required"),
  cache: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  maxMemory: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  lithography: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  price: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  detail1: yup.string().required("*Field is required"),
  detail2: yup.string().required("*Field is required"),
  detail3: yup.string().required("*Field is required"),
});

class CPUEditPage extends Component {
  constructor(props) {
    super(props); 
    this.state = {
      cpuId: props.match.params.id,
      cpu: {}
    };
  }

  componentDidMount() {
    this.getCPU();
  }

  async getCPU() {
    const res = await apiCaller.get(`/api/cpu/${this.state.cpuId}`);
    const cpu = res.data.payload;
    this.setState({ cpu });
  }

  handleSubmit = async(values) => {
    try {
      const formData = { ...values };
      console.log('FormData to be submitted', formData);
      await apiCaller.put(`api/cpu/update/${this.state.cpuId}`, formData);
      const res = await apiCaller.get(`/api/cpu/${this.state.cpuId}`);
      const cpu = res.data.payload;
      this.setState({ cpu });
      Toast.fire({
        icon: 'success',
        title: `Update Success: ${this.state.cpu.name}`
      })
      this.props.history.push('/cpu');
    } catch (error) {
      console.log("Failed to submit");
      throw error;
    }
  };

  render() {
    const { cpu } = this.state;
    console.log('Cpu data: ', cpu);
    return (
      <div className="content">
        <div className="banner-create">
          <h1>Edit CPU: {cpu.name}</h1>
        </div>
        <div className="backButton">
          <Link to="/cpu">
            <Button variant="warning" size="sm">Back</Button>
          </Link>
        </div>
        <div className="create-form">
          <Formik
            initialValues={cpu}
            enableReinitialize
            validationSchema={cpuSchema}
            onSubmit={(values, {setSubmitting, resetForm}) => {
              console.log("Form values: ", values);
              this.handleSubmit(values);
              setSubmitting(true);
              // resetForm();
              setSubmitting(false);
            }}
          >
            {({ touched, errors, values, handleChange, handleBlur, handleSubmit, isSubmitting, setFieldValue }) => (
              <Form onSubmit={handleSubmit}>
              {console.log(values)}
              <Row>
                <Group as={Col} md="4">
                  <Label>Name</Label>
                  <Control
                    type="text"
                    name="name"
                    size="sm"
                    placeholder="Name"
                    onChange={handleChange}
                    value={values.name}
                    isInvalid={!!errors.name}
                  />
                  {touched.name && errors.name ? <Feedback type="invalid">{errors.name}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Brand</Label>
                  <Control
                    type="text"
                    name="brand"
                    size="sm"
                    placeholder="Brand"
                    onChange={handleChange}
                    value={values.brand}
                    isInvalid={!!errors.brand}
                  />
                  {touched.brand && errors.brand ? <Feedback type="invalid">{errors.brand}</Feedback> : null}
                </Group>   
                <Group as={Col} md="4">
                  <Label>Model</Label>
                  <Control
                    type="text"
                    name="model"
                    size="sm"
                    placeholder="Model"
                    onChange={handleChange}
                    value={values.model}
                    isInvalid={!!errors.model}
                  />
                  {touched.model && errors.model ? <Feedback type="invalid">{errors.model}</Feedback> : null}
                </Group>   
              </Row>
              <Row className="mt-2"> 
                <Group as={Col} md="2">
                  <Label>Core Count</Label>
                  <Control
                    type="text"
                    name="coreCount"
                    size="sm"
                    placeholder="Core Count"
                    onChange={handleChange}
                    value={values.coreCount}
                    isInvalid={!!errors.coreCount}
                  />
                  {touched.coreCount && errors.coreCount ? <Feedback type="invalid">{errors.coreCount}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Clock Speed (Ghz)</Label>
                  <Control
                    type="text"
                    name="clockSpeed"
                    size="sm"
                    placeholder="Clock Speed (Ghz)"
                    onChange={handleChange}
                    value={values.clockSpeed}
                    isInvalid={!!errors.clockSpeed}
                  />
                  {touched.clockSpeed && errors.clockSpeed ? <Feedback type="invalid">{errors.clockSpeed}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Boost Clock (Ghz)</Label>
                  <Control
                    type="text"
                    name="boostClock"
                    size="sm"
                    placeholder="Boost Clock (Ghz)"
                    onChange={handleChange}
                    value={values.boostClock}
                    isInvalid={!!errors.boostClock}
                  />
                  {touched.boostClock && errors.boostClock ? <Feedback type="invalid">{errors.boostClock}</Feedback> : null}
                </Group>
                <Group as={Col} md="4">
                  <Label>Supported Socket</Label>
                  <Control
                    type="text"
                    name="supportedSocket"
                    size="sm"
                    placeholder="Suported Socket"
                    onChange={handleChange}
                    value={values.supportedSocket}
                    isInvalid={!!errors.supportedSocket}
                  />
                  {touched.supportedSocket && errors.supportedSocket ? <Feedback type="invalid">{errors.supportedSocket}</Feedback> : null}
                </Group>
              </Row >
              <Row className="mt-2">
                <Group as={Col} md="2">
                  <Label>No of Threads</Label>
                  <Control
                    type="text"
                    name="threads"
                    size="sm"
                    placeholder="No of Threads"
                    onChange={handleChange}
                    value={values.threads}
                    isInvalid={!!errors.threads}
                  />
                  {touched.threads && errors.threads ? <Feedback type="invalid">{errors.threads}</Feedback> : null}
                </Group>
                <Group as={Col} md="4">
                  <Label>Core Family</Label>
                  <Control
                    type="text"
                    name="coreFamily"
                    size="sm"
                    placeholder="Core Family"
                    onChange={handleChange}
                    value={values.coreFamily}
                    isInvalid={!!errors.coreFamily}
                  />
                  {touched.coreFamily && errors.coreFamily ? <Feedback type="invalid">{errors.coreFamily}</Feedback> : null}
                </Group>
                <Group as={Col} md="4">
                  <Label>Cache Size (MB)</Label>
                  <Control
                    type="text"
                    name="cache"
                    size="sm"
                    placeholder="Cache Size (MB)"
                    onChange={handleChange}
                    value={values.cache}
                    isInvalid={!!errors.cache}
                  />
                  {touched.cache && errors.cache ? <Feedback type="invalid">{errors.cache}</Feedback> : null}
                </Group>
              </Row>
              <Row className="mt-2">
                <Group as={Col} md="3">
                  <Label>Max Supported Memory (GB)</Label>
                  <Control
                    type="text"
                    name="maxMemory"
                    size="sm"
                    placeholder="Max Supported Memory (GB)"
                    onChange={handleChange}
                    value={values.maxMemory}
                    isInvalid={!!errors.maxMemory}
                  />
                  {touched.maxMemory && errors.maxMemory ? <Feedback type="invalid">{errors.maxMemory}</Feedback> : null}
                </Group>
                <Group as={Col} md="3">
                  <Label>Lithography (Nm)</Label>
                  <Control
                    type="text"
                    name="lithography"
                    size="sm"
                    placeholder="Lithography (Nm)"
                    onChange={handleChange}
                    value={values.lithography}
                    isInvalid={!!errors.lithography}
                  />
                  {touched.lithography && errors.lithography ? <Feedback type="invalid">{errors.lithography}</Feedback> : null}
                </Group>
                <Group as={Col} md="3">
                  <Label>Price</Label>
                  <Control
                    type="text"
                    name="price"
                    size="sm"
                    placeholder="Price"
                    onChange={handleChange}
                    value={values.price}
                    isInvalid={!!errors.price}
                  />
                  {touched.price && errors.price ? <Feedback type="invalid">{errors.price}</Feedback> : null}
                </Group>
              </Row>
              <Row>
                <Col md="5">
                  <Group>
                    <Label>Details 1</Label>
                    <Control
                      type="text"
                      name="detail1"
                      size="sm"
                      placeholder="Details 1"
                      onChange={handleChange}
                      value={values.detail1}
                      isInvalid={!!errors.detail1}
                    />
                    {touched.detail1 && errors.detail1 ? <Feedback type="invalid">{errors.detail1}</Feedback> : null}
                  </Group>
                  <Group>
                    <Label>Details 2</Label>
                    <Control
                      type="text"
                      name="detail2"
                      size="sm"
                      placeholder="Details 2"
                      onChange={handleChange}
                      value={values.detail2}
                      isInvalid={!!errors.detail2}
                    />
                    {touched.detail2 && errors.detail2 ? <Feedback type="invalid">{errors.detail2}</Feedback> : null}
                  </Group>
                  <Group>
                    <Label>Details 3</Label>
                    <Control
                      type="text"
                      name="detail3"
                      size="sm"
                      placeholder="Details 3"
                      onChange={handleChange}
                      value={values.detail3}
                      isInvalid={!!errors.detail3}
                    />
                    {touched.detail3 && errors.detail3 ? <Feedback type="invalid">{errors.detail3}</Feedback> : null}
                  </Group>
                </Col>
                <Col md="5">
                  <Group as={Col} md="4">
                    <Label>Upload Image</Label>
                    <Control
                      type="file"
                      name="file"
                      size="sm"
                      onChange={(e) => {
                        setFieldValue("file", e.currentTarget.files[0]);
                      }}
                    />
                  <Thumb file={values.file} />
                  </Group> 
                </Col>
              </Row>
              <Row className="mt-4">
                <Col>
                  <Button variant="primary" type="submit">
                    Submit & Update
                  </Button>
                </Col>
              </Row>
              </Form>           
            )}
          </Formik>
        </div>
      </div>
    );
  }
}

export default CPUEditPage;
