import React, { Component } from 'react';
import { Table, Space, Modal, Button } from 'antd';
import { Link } from 'react-router-dom';
import moment from 'moment';
import Swal from 'sweetalert2';
import apiCaller from '../../../tools/apiUtils';
import '../../../styles/MainContainer.scss';

class CoolerIndexPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cooler: []
    };
  }

  componentDidMount() {
    this.getCooler();
  }

  async getCooler() {
    try {
      const res = await apiCaller.get('api/cooler/');
      const cooler = res.data.payload;
      this.setState({ cooler });
      console.log('cooler Items', cooler)
    } catch (error) {
      console.log(error);
    }
  };

  deleteCooler(coolerId, coolerName) {
    const id = coolerId;
    const name = coolerName;
    Modal.confirm({
      title: `Are you sure to remove: ${name}`,
      okText: 'Yes, delete',
      okType: 'danger',
      cancelText: 'No',
      onOk: async() => {
        await apiCaller.delete(`api/cooler/delete/${id}`);
        Swal.fire({
          icon: 'success',
          title: 'Delete Success',
          text: `${name} deleted sucessfully!`
        });
        const res = await apiCaller.get('api/cooler/');
        const cooler = res.data.payload;
        this.setState({ cooler });
      }
    });
  };

  render(){
    const { cooler } = this.state;

    const columns = [
      {
        title: '#',
        dataIndex: 'id',
        width: '5%'
      },
      {
        title: 'Name',
        dataIndex: 'name',
        width: '35%'
      },
      {
        title: 'Brand',
        dataIndex: 'brand',
        width: '15%'
      },
      {
        title: 'Created At',
        dataIndex: 'createdAt',
        width: '13%',
        render: createdAt => (
          moment(createdAt).format('LL')
        )
      },
      {
        title: 'Price',
        dataIndex: 'price',
        width: '12%',
        render: price => (
          `RM ${price}`
        )
      },
      {
        title: 'Action',
        width: '20%',
        render: cooler => (
          <span>
            <Space>
              <Link to={`/cooler/edit/${cooler.id}`}>
                <Button type="primary" shape="round">
                  Edit & View
                </Button>
              </Link>
              <Button type="danger" shape="round" onClick={() => this.deleteCooler(cooler.id, cooler.name)}>
                Delete
              </Button>
            </Space>
          </span>
        )
      }
    ]

    console.log('cooler Items', cooler);
    return(
      <div className="content">
        <div className="banner">
          <h1>Manage PC Parts: CPU Cooler</h1> 
        </div>
        <div className="createButton">
          <Link to='/cooler/create'>
            <button type="button" className="btn btn-success">Add a CPU Cooler</button>
          </Link>
        </div>
        <div className="indexTable">
          <Table columns={columns} dataSource={cooler} pagination={{ defaultPageSize: 12 }} />
        </div>
      </div>
    );
  }
}



export default CoolerIndexPage;



