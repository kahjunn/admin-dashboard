import React, { Component } from 'react';
import { Button, Form, Col } from 'react-bootstrap';
import { Formik, Field, ErrorMessage } from 'formik';
import apiCaller from '../../../tools/apiUtils';
import Toast from '../../../tools/toaster';
import * as yup from 'yup';
import { Link, withRouter } from 'react-router-dom';
import '../../../styles/MainContainer.scss'
import '../styles/CoolerForm.scss';

const { Group, Label, Control, Row } = Form;
const { Feedback } = Form.Control;

const coolerSchema = yup.object().shape({
  name: yup.string().required("*Field is required"),
  brand: yup.string().required("*Field is required"),
  model: yup.string().required("*Field is required"),
  minFanRPM: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  maxFanRPM: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  coolerType: yup.string().required("*Selection is required"),
  color: yup.string().required("*Field is required"),
  intelSocket: yup.string(),
  amdSocket: yup.string(),
  price: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
  detail1: yup.string().required("*Field is required"),
  detail2: yup.string().required("*Field is required"),
  detail3: yup.string().required("*Field is required"),
});

class CoolerCreatePage extends Component {

  handleSubmit = async(values) => {
    try {
      const formData = { ...values };
      console.log('FormData to be submitted', formData);
      await apiCaller.post('api/cooler/create', formData);
      Toast.fire({
        icon: 'success',
        title: `Added Successfully: ${formData.name}`
      })
      this.props.history.push('/cooler');
    } catch (error) {
      console.log("Failed to submit");
      throw error;
    }
  };

  render() {
    return (
      <div className="cooler-content">
        <div className="banner-create">
          <h1>Add New CPU Cooler</h1>
        </div>
        <div className="backButton">
          <Link to="/cooler">
            <Button variant="warning" size="sm">Back</Button>
          </Link>
        </div>

        <div className="create-form">
          <Formik
            initialValues={{
              name: "",
              brand: "",
              model: "",
              minFanRPM: "",
              maxFanRPM: "",
              coolerType: "",
              color: "",
              intelSocket: "",
              amdSocket: "",
              price: "",
              detail1: "",
              detail2: "",
              detail3: ""
            }}
            validationSchema={coolerSchema}
            onSubmit={(values, {setSubmitting, resetForm}) => {
              console.log("Form values: ", values);
              this.handleSubmit(values);
              setSubmitting(true);
              // resetForm();
              setSubmitting(false);
            }}
          >
            {({ touched, errors, values, handleChange, handleBlur, handleSubmit, isSubmitting }) => (
              <Form onSubmit={handleSubmit}>
              {console.log(values)}
              <Row>
                <Group as={Col} md="4">
                  <Label>Name</Label>
                  <Control
                    type="text"
                    name="name"
                    size="sm"
                    placeholder="Name"
                    onChange={handleChange}
                    value={values.name}
                    isInvalid={!!errors.name}
                  />
                  {touched.name && errors.name ? <Feedback type="invalid">{errors.name}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Brand</Label>
                  <Control
                    type="text"
                    name="brand"
                    size="sm"
                    placeholder="Brand"
                    onChange={handleChange}
                    value={values.brand}
                    isInvalid={!!errors.brand}
                  />
                  {touched.brand && errors.brand ? <Feedback type="invalid">{errors.brand}</Feedback> : null}
                </Group>   
                <Group as={Col} md="4">
                  <Label>Model</Label>
                  <Control
                    type="text"
                    name="model"
                    size="sm"
                    placeholder="Model"
                    onChange={handleChange}
                    value={values.model}
                    isInvalid={!!errors.model}
                  />
                  {touched.model && errors.model ? <Feedback type="invalid">{errors.model}</Feedback> : null}
                </Group>   
              </Row>
              <Row className="mt-2"> 
                <Group as={Col} md="2">
                  <Label>Minimum Fan RPM</Label>
                  <Control
                    type="text"
                    name="minFanRPM"
                    size="sm"
                    placeholder="Minimum Fan RPM"
                    onChange={handleChange}
                    value={values.minFanRPM}
                    isInvalid={!!errors.minFanRPM}
                  />
                  {touched.minFanRPM && errors.minFanRPM ? <Feedback type="invalid">{errors.minFanRPM}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Maximum Fan RPM</Label>
                  <Control
                    type="text"
                    name="maxFanRPM"
                    size="sm"
                    placeholder="Maximum Fan RPM"
                    onChange={handleChange}
                    value={values.maxFanRPM}
                    isInvalid={!!errors.maxFanRPM}
                  />
                  {touched.maxFanRPM && errors.maxFanRPM ? <Feedback type="invalid">{errors.maxFanRPM}</Feedback> : null}
                </Group>
                <Group as={Col} md="2">
                  <Label>Cooler Type</Label>
                  <Control
                    as="select"
                    name="coolerType"
                    size="sm"
                    onChange={handleChange}
                    isInvalid={!!errors.coolerType}
                  >
                    <option selected disabled>Choose...</option>
                    <option values="Standard Air Cooler">Standard Air Cooler</option>
                    <option values="Tower Air Cooler">Tower Air Cooler</option>
                    <option values="AIO Liquid Cooler">AIO Liquid Cooler</option>
                  </Control>
                  {touched.coolerType && errors.coolerType ? <Feedback type="invalid">{errors.coolerType}</Feedback> : null}
                </Group>
                <Group as={Col} md="3">
                  <Label>Color</Label>
                  <Control
                    type="text"
                    name="color"
                    size="sm"
                    placeholder="Color"
                    onChange={handleChange}
                    value={values.color}
                    isInvalid={!!errors.color}
                  />
                  {touched.color && errors.color ? <Feedback type="invalid">{errors.color}</Feedback> : null}
                </Group>
              </Row >
              <Row className="mt-2">
                <Group as={Col} md="4">
                  <Label>Intel Supported Sockets</Label>
                  <Control
                    type="text"
                    name="intelSocket"
                    size="sm"
                    placeholder="Eg. LGA 1151, LGA 1150..."
                    onChange={handleChange}
                    value={values.intelSocket}
                    isInvalid={!!errors.intelSocket}
                  />
                  {touched.intelSocket && errors.intelSocket ? <Feedback type="invalid">{errors.intelSocket}</Feedback> : null}
                </Group>
                <Group as={Col} md="4">
                  <Label>AMD Supported Sockets</Label>
                  <Control
                    type="text"
                    name="amdSocket"
                    size="sm"
                    placeholder="Eg. AM3, AM4..."
                    onChange={handleChange}
                    value={values.amdSocket}
                    isInvalid={!!errors.amdSocket}
                  />
                  {touched.amdSocket && errors.amdSocket ? <Feedback type="invalid">{errors.amdSocket}</Feedback> : null}
                </Group>
              </Row>
              <Row className="mt-2">
                <Group as={Col} md="3">
                  <Label>Price (RM)</Label>
                  <Control
                    type="text"
                    name="price"
                    size="sm"
                    placeholder="Price"
                    onChange={handleChange}
                    value={values.price}
                    isInvalid={!!errors.price}
                  />
                  {touched.price && errors.price ? <Feedback type="invalid">{errors.price}</Feedback> : null}
                </Group>
              </Row>
              <Row>
                <Col md="5">
                  <Group>
                    <Label>Details 1</Label>
                    <Control
                      type="text"
                      name="detail1"
                      size="sm"
                      placeholder="Details 1"
                      onChange={handleChange}
                      value={values.detail1}
                      isInvalid={!!errors.detail1}
                    />
                    {touched.detail1 && errors.detail1 ? <Feedback type="invalid">{errors.detail1}</Feedback> : null}
                  </Group>
                  <Group>
                    <Label>Details 2</Label>
                    <Control
                      type="text"
                      name="detail2"
                      size="sm"
                      placeholder="Details 2"
                      onChange={handleChange}
                      value={values.detail2}
                      isInvalid={!!errors.detail2}
                    />
                    {touched.detail2 && errors.detail2 ? <Feedback type="invalid">{errors.detail2}</Feedback> : null}
                  </Group>
                  <Group>
                    <Label>Details 3</Label>
                    <Control
                      type="text"
                      name="detail3"
                      size="sm"
                      placeholder="Details 3"
                      onChange={handleChange}
                      value={values.detail3}
                      isInvalid={!!errors.detail3}
                    />
                    {touched.detail3 && errors.detail3 ? <Feedback type="invalid">{errors.detail3}</Feedback> : null}
                  </Group>
                </Col>
              </Row>
              <Row className="mt-4">
                <Col>
                  <Button variant="primary" type="submit">
                    Submit
                  </Button>
                </Col>
              </Row>
              </Form>           
            )}
          </Formik>
        </div>
      </div>
    );
  }
}

export default CoolerCreatePage;
