import React, { Component } from 'react';
import { Table } from 'antd';

class ListContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: props.columns,
      style: props.style,
      dataSource: props.dataSource,
    } 
      
  }
  render() {
    const { columns, style, dataSource } = this.state;
    return (
      <Table
        size="middle"
        columns={columns}
        style={style}
        dataSource={dataSource}
      />
    )
  }
}

export default ListContainer;