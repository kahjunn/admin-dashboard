import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import ScrollToTop from './modules/App/components/ScrollToTop';
import Sidebar from './modules/App/components/Sidebar';
import Header from './modules/App/components/Header';
import AdminIndexPage from './modules/Admin/components/AdminIndexPage';
import AdminCreatePage from './modules/Admin/components/AdminCreatePage';
import AdminEditPage from './modules/Admin/components/AdminEditPage';
import CPUIndexPage from './modules/CPU/components/CPUIndexPage';
import CPUCreatePage from './modules/CPU/components/CPUCreatePage';
import CPUEditPage from './modules/CPU/components/CPUEditPage';
import GPUIndexPage from './modules/GPU/components/GPUIndexPage';
import GPUCreatePage from './modules/GPU/components/GPUCreatePage';
import GPUEditPage from './modules/GPU/components/GPUEditPage';
import MOBOIndexPage from './modules/Motherboard/components/MOBOIndexPage';
import MOBOCreatePage from './modules/Motherboard/components/MOBOCreatePage';
import MOBOEditPage from './modules/Motherboard/components/MOBOEditPage';
import RAMIndexPage from './modules/RAM/components/RAMIndexPage';
import RAMCreatePage from './modules/RAM/components/RAMCreatePage';
import RAMEditPage from './modules/RAM/components/RAMEditPage';
import StorageIndexPage from './modules/Storage/components/StorageIndexPage';
import StorageCreatePage from './modules/Storage/components/StorageCreatePage';
import StorageEditPage from './modules/Storage/components/StorageEditPage';
import PowerIndexPage from './modules/Power/components/PowerIndexPage';
import PowerCreatePage from './modules/Power/components/PowerCreatePage';
import PowerEditPage from './modules/Power/components/PowerEditPage';
import CoolerIndexPage from './modules/Cooler/components/CoolerIndexPage';
import CoolerCreatePage from './modules/Cooler/components/CoolerCreatePage';
import CoolerEditPage from './modules/Cooler/components/CoolerEditPage';
import CasingIndexPage from './modules/Casing/components/CasingIndexPage';
import CasingCreatePage from './modules/Casing/components/CasingCreatePage';
import CasingEditPage from './modules/Casing/components/CasingEditPage';
import BuildsIndexPage from './modules/PCBuilds/components/BuildsIndexPage';
import BuildsCreatePage from './modules/PCBuilds/components/BuildsCreatePage';
import BuildsEditPage from './modules/PCBuilds/components/BuildsEditPage';
import BOIndexPage from './modules/BuildOrder/components/BOIndexPage';
import BOEditPage from './modules/BuildOrder/components/BOEditPage';
import PromoCreatePage from './modules/Promotion/components/PromoCreatePage';
import PromoEditPage from './modules/Promotion/components/PromoEditPage';
import PromoIndexPage from './modules/Promotion/components/PromoIndexPage';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <ScrollToTop>
          <Sidebar />
          <Header />
            <Route exact path="/admin" component={AdminIndexPage} />
            <Route exact path="/admin/create" component={AdminCreatePage} />
            <Route exact path="/admin/edit/:id" component={AdminEditPage} />
            <Route exact path="/cpu" component={CPUIndexPage} />
            <Route exact path="/cpu/create" component={CPUCreatePage} />
            <Route exact path="/cpu/edit/:id" component={CPUEditPage} />
            <Route exact path="/gpu" component={GPUIndexPage} />
            <Route exact path="/gpu/create" component={GPUCreatePage} />
            <Route exact path="/gpu/edit/:id" component={GPUEditPage} />
            <Route exact path="/mobo" component={MOBOIndexPage} />
            <Route exact path="/mobo/create" component={MOBOCreatePage} />
            <Route exact path="/mobo/edit/:id" component={MOBOEditPage} />
            <Route exact path="/ram" component={RAMIndexPage} />
            <Route exact path="/ram/create" component={RAMCreatePage} />
            <Route exact path="/ram/edit/:id" component={RAMEditPage} />
            <Route exact path="/storage" component={StorageIndexPage} />
            <Route exact path="/storage/create" component={StorageCreatePage} />
            <Route exact path="/storage/edit/:id" component={StorageEditPage} />
            <Route exact path="/power" component={PowerIndexPage} />
            <Route exact path="/power/create" component={PowerCreatePage} />
            <Route exact path="/power/edit/:id" component={PowerEditPage} />
            <Route exact path="/cooler" component={CoolerIndexPage} />
            <Route exact path="/cooler/create" component={CoolerCreatePage} />
            <Route exact path="/cooler/edit/:id" component={CoolerEditPage} />
            <Route exact path="/casing" component={CasingIndexPage} />
            <Route exact path="/casing/create" component={CasingCreatePage} />
            <Route exact path="/casing/edit/:id" component={CasingEditPage} />
            <Route exact path="/pcbuilds" component={BuildsIndexPage} />
            <Route exact path="/pcbuilds/create" component={BuildsCreatePage} />
            <Route exact path="/pcbuilds/edit/:id" component={BuildsEditPage} />
            <Route exact path="/bo" component={BOIndexPage} />
            <Route exact path="/bo/edit/:id" component={BOEditPage} />
            <Route exact path="/promotion" component={PromoIndexPage} />
            <Route exact path="/promotion/create" component={PromoCreatePage} />
            <Route exact path="/promotion/edit/:id" component={PromoEditPage} />
          </ScrollToTop>
        </Router>
      </div>
    )
  }
}

export default App;
